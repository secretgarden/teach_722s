//start
var res_start = {
	start_g : "res/start.png",
	start_p : "res/start.plist",
	bg_mp3 : "res/music/bg.mp3",
	logo1_png : "res/logo1.png",
	duang1 : "res/Duang/duang1.plist",
	duang2 : "res/Duang/duang2.plist",
	follow_g : "res/Duang/follow.png",
	follow_p : "res/Duang/follow.plist"
};

var g_resources_start = [];
for (var i in res_start) {
	g_resources_start.push(res_start[i]);
}
// run
var res_run = {
	run_p : "res/run.plist",
	run_g : "res/run.png",
	red_run_p : "res/redRun.plist",
	red_run_g : "res/redRun.png",
	c7_p : "res/C7.plist",
	c7_g : "res/C7.png",
	note_bg : "res/note_bg.png",
	logo : "res/logo.png",
	ok_mp3 : "res/music/ok.mp3",
	font_f : "res/fonts/teach.fnt",
	font_g : "res/fonts/teach_0.png",
	zhegt_out_g : "res/Action/zhegt_out0.png",
	zhegt_out_p : "res/Action/zhegt_out0.plist",
	zhegt_out_j : "res/Action/zhegt_out.ExportJson",
	canbi_g : "res/Action/canbi0.png",
	canbi_p : "res/Action/canbi0.plist",
	canbi_j : "res/Action/canbi.ExportJson",
	error_mp3 : "res/music/error.wav",
	clock_mp3 : "res/music/clock.wav"
};

var g_resources_run = [];
for (var i in res_run) {
	g_resources_run.push(res_run[i]);
}
// finish
var res_finish = {
	finish_g : "res/finish.png",
	finish_p : "res/finish.plist",
	over_mp3 : "res/music/over.mp3",
	firework_g : "res/Duang/firework.png",
	firework_p : "res/Duang/firework.plist"
};

var g_resources_finish = [];
for (var i in res_finish) {
	g_resources_finish.push(res_finish[i]);
}

// game
var res_game = {
	game_p: "res/game.plist",
	game_g: "res/game.png",
	spark: "res/Duang/spark.plist",
	star1d: "res/Duang/star1d.png",
	broken: "res/music/broken.mp3",
	win: "res/music/win.mp3"
};
var g_resources_game = [];
for (var i in res_game) {
	g_resources_game.push(res_game[i]);
}

var res_data = {
	wp:"res/wp.png",
	data_bg:"res/data_bg.jpg"
};
var g_resources_data = [];
for (var i in res_data) {
	g_resources_data.push(res_data[i]);
}

