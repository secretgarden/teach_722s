var DataMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
	curSel:0,
	textArr: null,
	imageArr : null,
	tagArr1: null,
	tagArr2: null,
	tagVal: null,
	angelArr: [],
	def_c:null,
	lineWidth:0,
	dataSer:0,
	dataSer2:0,
    ctor:function () {
        this._super();
        this.init();
    },
    init:function(){
    	this.initData();
    	this.def_c = cc.color(0,0,0,200);
    	this.lineWidth = 2;
    	this.dataSer = 0;
    	this.dataSer2 = 0;
    	this.loadBack();
    	// 画表格
    	this.loadFrame();
    	this.scheduleOnce(function(){
    		this.loadData2();
    	},0.2);
    	this.scheduleOnce(function(){
    		this.loadTitle();
    	},1);
		this.scheduleOnce(function(){
			this.loadTable();
			this.loadData();
		},1.5);
    },
    loadData:function(){
    	// 图
    	if(this.dataSer >= this.array.length){
    		// 结束
    		return;
    	}
    	var cur = this.array[this.dataSer++];
    	this.genLabel(cur.name,cur.pos,this.loadData,cur.dp);
    },
    loadData2:function(){
    	// 表
    	if(this.dataSer2 >= this.array2.length){
    		// 结束
    		this.drawingLine(cc.p(356, 341),cc.p(614, 496),3, cc.color(255,0,0));
    		this.scheduleOnce(function(){
    			// 公式
    			this.loadExp();
    		},0.8);
    		return;
    	}
    	for(var i = 0; i < 5; i++){
    		var cur = this.array2[this.dataSer2++];
    		this.genLabel2(cur.name,cur.pos,this.loadData2,cur.dp);	
    	}
    },
    genLabel2:function(str,pos,callback,dp){
    	var label = new cc.LabelTTF(str,gg.fontName, 16);
    	label.setAnchorPoint(0,0)
    	label.setPosition(-100,pos.y);
    	this.addChild(label);
    	var move = cc.moveTo(0.5,pos);
    	var func = cc.callFunc(function(){
    		if(dp != null){
    			var dn = new cc.DrawNode();
    			dn.drawDot(dp,5,cc.color(0,0,0));
    			this.addChild(dn);
    			callback.call(this,this);
    		}
    	}.bind(this),this);
    	var seq = cc.sequence(move,func);
    	label.runAction(seq);
    },
    genLabel:function(str,pos,callback,dp){
    	var label = new cc.LabelTTF(str,gg.fontName, 16);
    	label.setAnchorPoint(0,0)
    	label.setOpacity(0);
    	label.setPosition(pos);
    	this.addChild(label);
    	var fade = cc.fadeTo(0.4, 255);
    	var func = cc.callFunc(function(){
    		callback.call(this,this);	
    	}.bind(this),this);
    	var seq = cc.sequence(fade,func);
    	label.runAction(seq);
    },
    loadExp:function(){
    	var label = new cc.LabelTTF("y = 0.378x + 0.0268",gg.fontName, 22);
    	label.setPosition(gg.c_width,772);
    	this.addChild(label);
    	var move = cc.moveTo(1,cc.p(gg.c_width,520));
    	var jump = cc.jumpTo(2,cc.p(gg.c_width,520),30,3);
    	var seq = cc.sequence(move, jump);
    	label.runAction(seq);
    },
    loadTitle:function(){
    	var label = new cc.LabelTTF("甲醛标准曲线",gg.fontName, 25);
    	label.setOpacity(0);
    	label.setPosition(gg.c_width,290);
    	this.addChild(label);
    	label.runAction(cc.fadeTo(0.8,255));
    },
    loadTable:function(){
    	this.drawingLine(cc.p(356, 341),cc.p(356, 541));
    	this.drawingLine(cc.p(356, 341),cc.p(680, 341));
    },
    loadFrame:function(){
		// 6条横线
		var y = 104, margin = 30;
		for(var i=0;i<6;i++){
			this.drawingLine(cc.p(194, y),cc.p(788, y));
			y += margin;
		}

		// 11条竖线
		var x = 194,margin = 54;
		this.drawingLine(cc.p(x, 104),cc.p(x, 254));
		x += margin;
		x += margin;
		for(var i = 0;i<10;i++){
			this.drawingLine(cc.p(x, 104),cc.p(x, 254));
			x += margin;
		}
    },
    drawingLine:function(begin, end, width, color){
    	if(color == null){
    		color = this.def_c;
    	}
    	if(width == null){
    		width = this.lineWidth;
    	}
    	var per = 4,perX = 0,perY = 0;
    	var tx = begin.x > end.x ? begin.x - end.x : end.x - begin.x;
    	var ty = begin.y > end.y ? begin.y - end.y : end.y - begin.y;
    	var dn = new cc.DrawNode();
    	dn.setDrawColor(color);
    	dn.setLineWidth(width)
    	this.addChild(dn);
    		if(ty > tx){
    			// Y斜线
    			repeat = ty / per;
    			perY = begin.y > end.y ? -per : per;
    			perX = (tx / ty) * per;
    		} else {
    			// X斜线
    			repeat = tx / per;
    			perX = begin.x > end.x ? -per : per;
    			perY = (ty / tx) * per;
    		}
    		dn.schedule(function(){
    			begin.y = begin.y + perY;
    			begin.x = begin.x + perX;
    			dn.drawSegment(cc.p(begin.x - perX,begin.y - perY),cc.p(begin.x,begin.y));
    		}, 0.01, repeat);
    },
    initData:function(){
    	var x1 = 356 - 42, y1 = 341 - 21, m1 = 40, m2 = 54;
    	var x2 = 200,y2 = 230, m3 = 30, m4 = 54;
		this.array = [{name:"1.000",pos:cc.p(x1, y1 + m1 * 5)},{name:"0.800",pos:cc.p(x1, y1 + m1 * 4)},{name:"0.600",pos:cc.p(x1, y1 + m1 * 3)},
					{name:"0.400",pos:cc.p(x1, y1 + m1 * 2)},{name:"0.200",pos:cc.p(x1, y1 + m1)},{name:"0.000",pos:cc.p(x1, y1)},
					{name:"0.400",pos:cc.p(x1 + m2, y1)},{name:"0.800",pos:cc.p(x1 + m2 * 2, y1)},{name:"1.200",pos:cc.p(x1 + m2 * 3, y1)},
					{name:"1.600",pos:cc.p(x1 + m2 * 4, y1)},{name:"2.000",pos:cc.p(x1 + m2 * 5, y1)},{name:"2.400",pos:cc.p(x1 + m2 * 6, y1)}];
		this.array2 = [{name:"管号",pos:cc.p(x2, y2)},{name:"标液,ml",pos:cc.p(x2, y2 - m3)},{name:"吸收液,ml",pos:cc.p(x2, y2 - m3 * 2)},{name:"甲醛含量,ug",pos:cc.p(x2, y2 - m3 * 3)},{name:"吸光度A",pos:cc.p(x2, y2 - m3 * 4),dp:cc.p(-10,-10)},
					{name:"0",pos:cc.p(x2 + m4 * 2, y2)},{name:"0.00",pos:cc.p(x2 + m4 * 2, y2 - m3)},{name:"5.00",pos:cc.p(x2 + m4 * 2, y2 - m3 * 2)},{name:"0.00",pos:cc.p(x2 + m4 * 2, y2 - m3 * 3)},{name:"0.018",pos:cc.p(x2 + m4 * 2, y2 - m3 * 4),dp:cc.p(356,341)},
					{name:"1",pos:cc.p(x2 + m4 * 3, y2)},{name:"0.10",pos:cc.p(x2 + m4 * 3, y2 - m3)},{name:"4.90",pos:cc.p(x2 + m4 * 3, y2 - m3 * 2)},{name:"0.10",pos:cc.p(x2 + m4 * 3, y2 - m3 * 3)},{name:"0.068",pos:cc.p(x2 + m4 * 3, y2 - m3 * 4),dp:cc.p(375,352)},
					{name:"2",pos:cc.p(x2 + m4 * 4, y2)},{name:"0.20",pos:cc.p(x2 + m4 * 4, y2 - m3)},{name:"4.80",pos:cc.p(x2 + m4 * 4, y2 - m3 * 2)},{name:"0.20",pos:cc.p(x2 + m4 * 4, y2 - m3 * 3)},{name:"0.103",pos:cc.p(x2 + m4 * 4, y2 - m3 * 4),dp:cc.p(386,356)},                                  
					{name:"3",pos:cc.p(x2 + m4 * 5, y2)},{name:"0.40",pos:cc.p(x2 + m4 * 5, y2 - m3)},{name:"4.60",pos:cc.p(x2 + m4 * 5, y2 - m3 * 2)},{name:"0.40",pos:cc.p(x2 + m4 * 5, y2 - m3 * 3)},{name:"0.179",pos:cc.p(x2 + m4 * 5, y2 - m3 * 4),dp:cc.p(401,372)},
					{name:"4",pos:cc.p(x2 + m4 * 6, y2)},{name:"0.60",pos:cc.p(x2 + m4 * 6, y2 - m3)},{name:"4.40",pos:cc.p(x2 + m4 * 6, y2 - m3 * 2)},{name:"0.60",pos:cc.p(x2 + m4 * 6, y2 - m3 * 3)},{name:"0.252",pos:cc.p(x2 + m4 * 6, y2 - m3 * 4),dp:cc.p(427,382)},
					{name:"5",pos:cc.p(x2 + m4 * 7, y2)},{name:"0.80",pos:cc.p(x2 + m4 * 7, y2 - m3)},{name:"4.20",pos:cc.p(x2 + m4 * 7, y2 - m3 * 2)},{name:"0.80",pos:cc.p(x2 + m4 * 7, y2 - m3 * 3)},{name:"0.335",pos:cc.p(x2 + m4 * 7, y2 - m3 * 4),dp:cc.p(453,398)},
					{name:"6",pos:cc.p(x2 + m4 * 8, y2)},{name:"1.00",pos:cc.p(x2 + m4 * 8, y2 - m3)},{name:"4.00",pos:cc.p(x2 + m4 * 8, y2 - m3 * 2)},{name:"1.00",pos:cc.p(x2 + m4 * 8, y2 - m3 * 3)},{name:"0.409",pos:cc.p(x2 + m4 * 8, y2 - m3 * 4),dp:cc.p(482,414)},
					{name:"7",pos:cc.p(x2 + m4 * 9, y2)},{name:"1.50",pos:cc.p(x2 + m4 * 9, y2 - m3)},{name:"3.50",pos:cc.p(x2 + m4 * 9, y2 - m3 * 2)},{name:"1.50",pos:cc.p(x2 + m4 * 9, y2 - m3 * 3)},{name:"0.593",pos:cc.p(x2 + m4 * 9, y2 - m3 * 4),dp:cc.p(540,447)},
					{name:"8",pos:cc.p(x2 + m4 * 10, y2)},{name:"2.00",pos:cc.p(x2 + m4 * 10, y2 - m3)},{name:"3.00",pos:cc.p(x2 + m4 * 10, y2 - m3 * 2)},{name:"2.00",pos:cc.p(x2 + m4 * 10, y2 - m3 * 3)},{name:"0.779",pos:cc.p(x2 + m4 * 10, y2 - m3 * 4),dp:cc.p(614,496)}]
    },
    loadBack:function(){
    	var back = new Angel(this,"#game_back.png",function(){
    		$.runScene(new StartScene());
    	},this);
    	back.setPosition(10 + back.width * 0.5, 10 + back.height * 0.5);
    }
});

