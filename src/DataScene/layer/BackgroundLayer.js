var DataBackgroundLayer = cc.Layer.extend({
    ctor:function () {
        this._super();
        this.loadBg();
        return true;
    },
    loadBg : function(){
    	var node = new cc.Sprite(res_data.data_bg);
        this.addChild(node);
        node.setPosition(gg.c_p);
    }
});