var DataMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
	curSel:0,
	textArr: null,
	imageArr : null,
	tagArr1: null,
	tagArr2: null,
	tagVal: null,
	angelArr: [],
	def_c:null,
	lineWidth:0,
	dataSer:0,
    ctor:function () {
        this._super();
        this.init();
    },
    init:function(){
    	this.initData();
    	this.def_c = cc.color(0,0,0,200);
    	this.lineWidth = 2;
    	this.dataSer = 0;
    	this.loadBack();
    	this.loadFixed();
    	this.loadFrame();
    	this.loadTable();
    	this.loadTitle();
    	this.loadData();
    },
    loadFixed:function(){
    	for(var i in this.array){
    		var cur = this.array[i];
    		this.genLabel(cur.name,cur.pos);	
    	}
    	for(var i in this.array2){
    		var cur = this.array2[i];
    		this.genLabel(cur.name,cur.pos);	
    	}
    },
    loadData:function(){
    	var intelval = 1;
    	intelval = 0.5; // TODO
    	// 表
    	if(this.dataSer >= this.array3.length){
    		// 结束
    		this.drawingLine(cc.p(356, 341),cc.p(614, 496),3, cc.color(255,0,0));
    		this.scheduleOnce(function(){
    			// 公式
    			this.loadExp();
    		}, intelval);
    		return;
    	}
		var cur = this.array3[this.dataSer++];
		this.genLabel2(cur.name,cur.pos,this.loadData,cur.dp);
    },
    genLabel2:function(str,pos,callback,dp){
    	var label = new cc.LabelTTF(str,gg.fontName, 16);
    	label.setAnchorPoint(0,0)
    	label.setPosition(-100,pos.y);
    	this.addChild(label);
    	var time = 0.5;
    	time = 0.25; // TODO
    	var move = cc.moveTo(time, pos);
    	var func = cc.callFunc(function(){
    		if(dp != null){
    			var wp = new cc.Sprite(res_data.wp);
    			wp.setPosition(dp);
    			this.addChild(wp);
    			var intelval = 0.2;
    			intelval = 0.1; // TODO
    			var seq = cc.sequence(cc.fadeTo(intelval,100),cc.fadeTo(intelval,255));
    			var seq2 = cc.sequence(cc.scaleTo(intelval,2),cc.scaleTo(intelval,1));
    			var spawn = cc.spawn(seq, seq2);
    			var repeat = cc.repeat(spawn, 3);
    			var seq3 = cc.sequence(repeat,cc.callFunc(function(){
    				callback.call(this,this);
    			},this));
    			wp.runAction(seq3);
    		}
    	}.bind(this),this);
    	var seq = cc.sequence(move,func);
    	label.runAction(seq);
    },
    genLabel:function(str,pos){
    	var label = new cc.LabelTTF(str,gg.fontName, 16);
    	label.setAnchorPoint(0,0)
    	label.setOpacity(0);
    	label.setPosition(pos);
    	this.addChild(label);
    	var fade = cc.fadeTo(0.4, 255);
    	label.runAction(fade);
    },
    loadExp:function(){
    	var label = new cc.LabelTTF("y = 0.378x + 0.0268",gg.fontName, 22);
    	label.setPosition(gg.c_width,772);
    	this.addChild(label);
    	var intelval = 1;
    	intelval = 0.5; // TODO
    	var move = cc.moveTo(intelval,cc.p(gg.c_width,520));
    	var jump = cc.jumpTo(intelval * 2,cc.p(gg.c_width,520),30,3);
    	var seq = cc.sequence(move, jump);
    	label.runAction(seq);
    	
    	var label2 = new cc.LabelTTF("相关系数T:\n0.9998",gg.fontName, 22);
    	label2.setPosition(800, 450);
    	label2.setOpacity(0);
    	this.addChild(label2);
    	label2.runAction(cc.fadeTo(0.5,255));
    },
    loadTitle:function(){
    	var label = new cc.LabelTTF("甲醛标准曲线",gg.fontName, 25);
    	label.setOpacity(0);
    	label.setPosition(gg.c_width,290);
    	this.addChild(label);
    	label.runAction(cc.fadeTo(0.8,255));
    },
    loadTable:function(){
    	var dn = new cc.DrawNode();
    	dn.setDrawColor(this.def_c);
    	dn.setLineWidth(this.lineWidth);
    	this.addChild(dn);
    	
    	dn.drawSegment(cc.p(356, 341),cc.p(356, 581),this.lineWidth,this.def_c);
    	dn.drawSegment(cc.p(356, 341),cc.p(734, 341),this.lineWidth,this.def_c);
    },
    loadFrame:function(){
    	var dn = new cc.DrawNode();
    	this.addChild(dn);
		// 6条横线
		var y = 104, margin = 30;
		for(var i=0;i<6;i++){
			dn.drawSegment(cc.p(194, y),cc.p(788, y),this.lineWidth,this.def_c);
			y += margin;
		}

		// 11条竖线
		var x = 194,margin = 54;
		dn.drawSegment(cc.p(x, 104),cc.p(x, 254),this.lineWidth,this.def_c);
		x += margin;
		x += margin;
		for(var i = 0;i<10;i++){
			dn.drawSegment(cc.p(x, 104),cc.p(x, 254),this.lineWidth,this.def_c);
			x += margin;
		}
    },
    drawingLine:function(begin, end, width, color){
    	if(color == null){
    		color = this.def_c;
    	}
    	if(width == null){
    		width = this.lineWidth;
    	}
    	var per = 4,perX = 0,perY = 0;
    	per = 8; // TODO
    	var tx = begin.x > end.x ? begin.x - end.x : end.x - begin.x;
    	var ty = begin.y > end.y ? begin.y - end.y : end.y - begin.y;
    	var dn = new cc.DrawNode();
    	this.addChild(dn);
    		if(ty > tx){
    			// Y斜线
    			repeat = ty / per;
    			perY = begin.y > end.y ? -per : per;
    			perX = (tx / ty) * per;
    		} else {
    			// X斜线
    			repeat = tx / per;
    			perX = begin.x > end.x ? -per : per;
    			perY = (ty / tx) * per;
    		}
    		dn.schedule(function(){
    			begin.y = begin.y + perY;
    			begin.x = begin.x + perX;
    			dn.drawSegment(cc.p(begin.x - perX,begin.y - perY),cc.p(begin.x,begin.y), width, color);
    		}, 0.01, repeat);
    },
    initData:function(){
    	var x1 = 356 - 42, y1 = 341 - 21, m1 = 40, m2 = 54;
    	var x2 = 200,y2 = 230, m3 = 30, m4 = 54;
    	
    	this.array = [{name:"吸光度A",pos:cc.p(x1 - 20, y1 + m1 * 6)},{name:"1.000",pos:cc.p(x1, y1 + m1 * 5)},{name:"0.800",pos:cc.p(x1, y1 + m1 * 4)},{name:"0.600",pos:cc.p(x1, y1 + m1 * 3)},
					{name:"0.400",pos:cc.p(x1, y1 + m1 * 2)},{name:"0.200",pos:cc.p(x1, y1 + m1)},{name:"0.000",pos:cc.p(x1, y1)},
					{name:"0.400",pos:cc.p(x1 + m2, y1)},{name:"0.800",pos:cc.p(x1 + m2 * 2, y1)},{name:"1.200",pos:cc.p(x1 + m2 * 3, y1)},
					{name:"1.600",pos:cc.p(x1 + m2 * 4, y1)},{name:"2.000",pos:cc.p(x1 + m2 * 5, y1)},{name:"2.400",pos:cc.p(x1 + m2 * 6, y1)},{name:"甲醛浓度\n    μg",pos:cc.p(x1 + m2 * 7, y1 - 22)}];
		this.array2 = [{name:"管号",pos:cc.p(x2, y2)},{name:"标液,ml",pos:cc.p(x2, y2 - m3)},{name:"吸收液,ml",pos:cc.p(x2, y2 - m3 * 2)},{name:"甲醛含量,ug",pos:cc.p(x2, y2 - m3 * 3)},{name:"吸光度A",pos:cc.p(x2, y2 - m3 * 4)},
					{name:"0",pos:cc.p(x2 + m4 * 2, y2)},{name:"0.00",pos:cc.p(x2 + m4 * 2, y2 - m3)},{name:"5.00",pos:cc.p(x2 + m4 * 2, y2 - m3 * 2)},{name:"0.00",pos:cc.p(x2 + m4 * 2, y2 - m3 * 3)},
					{name:"1",pos:cc.p(x2 + m4 * 3, y2)},{name:"0.10",pos:cc.p(x2 + m4 * 3, y2 - m3)},{name:"4.90",pos:cc.p(x2 + m4 * 3, y2 - m3 * 2)},{name:"0.10",pos:cc.p(x2 + m4 * 3, y2 - m3 * 3)},
					{name:"2",pos:cc.p(x2 + m4 * 4, y2)},{name:"0.20",pos:cc.p(x2 + m4 * 4, y2 - m3)},{name:"4.80",pos:cc.p(x2 + m4 * 4, y2 - m3 * 2)},{name:"0.20",pos:cc.p(x2 + m4 * 4, y2 - m3 * 3)},                                  
					{name:"3",pos:cc.p(x2 + m4 * 5, y2)},{name:"0.40",pos:cc.p(x2 + m4 * 5, y2 - m3)},{name:"4.60",pos:cc.p(x2 + m4 * 5, y2 - m3 * 2)},{name:"0.40",pos:cc.p(x2 + m4 * 5, y2 - m3 * 3)},
					{name:"4",pos:cc.p(x2 + m4 * 6, y2)},{name:"0.60",pos:cc.p(x2 + m4 * 6, y2 - m3)},{name:"4.40",pos:cc.p(x2 + m4 * 6, y2 - m3 * 2)},{name:"0.60",pos:cc.p(x2 + m4 * 6, y2 - m3 * 3)},
					{name:"5",pos:cc.p(x2 + m4 * 7, y2)},{name:"0.80",pos:cc.p(x2 + m4 * 7, y2 - m3)},{name:"4.20",pos:cc.p(x2 + m4 * 7, y2 - m3 * 2)},{name:"0.80",pos:cc.p(x2 + m4 * 7, y2 - m3 * 3)},
					{name:"6",pos:cc.p(x2 + m4 * 8, y2)},{name:"1.00",pos:cc.p(x2 + m4 * 8, y2 - m3)},{name:"4.00",pos:cc.p(x2 + m4 * 8, y2 - m3 * 2)},{name:"1.00",pos:cc.p(x2 + m4 * 8, y2 - m3 * 3)},
					{name:"7",pos:cc.p(x2 + m4 * 9, y2)},{name:"1.50",pos:cc.p(x2 + m4 * 9, y2 - m3)},{name:"3.50",pos:cc.p(x2 + m4 * 9, y2 - m3 * 2)},{name:"1.50",pos:cc.p(x2 + m4 * 9, y2 - m3 * 3)},
					{name:"8",pos:cc.p(x2 + m4 * 10, y2)},{name:"2.00",pos:cc.p(x2 + m4 * 10, y2 - m3)},{name:"3.00",pos:cc.p(x2 + m4 * 10, y2 - m3 * 2)},{name:"2.00",pos:cc.p(x2 + m4 * 10, y2 - m3 * 3)},]
		this.array3 = [{name:"0.018",pos:cc.p(x2 + m4 * 2, y2 - m3 * 4),dp:cc.p(356,341)},
					{name:"0.068",pos:cc.p(x2 + m4 * 3, y2 - m3 * 4),dp:cc.p(375,352)},
					{name:"0.103",pos:cc.p(x2 + m4 * 4, y2 - m3 * 4),dp:cc.p(386,356)},
					{name:"0.179",pos:cc.p(x2 + m4 * 5, y2 - m3 * 4),dp:cc.p(401,372)},
					{name:"0.252",pos:cc.p(x2 + m4 * 6, y2 - m3 * 4),dp:cc.p(427,382)},
					{name:"0.335",pos:cc.p(x2 + m4 * 7, y2 - m3 * 4),dp:cc.p(453,398)},
					{name:"0.409",pos:cc.p(x2 + m4 * 8, y2 - m3 * 4),dp:cc.p(482,414)},
					{name:"0.593",pos:cc.p(x2 + m4 * 9, y2 - m3 * 4),dp:cc.p(540,447)},
					{name:"0.779",pos:cc.p(x2 + m4 * 10, y2 - m3 * 4),dp:cc.p(614,496)}];
    },
    loadBack:function(){
    	var back = new Angel(this,"#game_back.png",function(){
    		$.runScene(new StartScene());
    	},this);
    	back.setPosition(10 + back.width * 0.5, 10 + back.height * 0.5);
    }
});

