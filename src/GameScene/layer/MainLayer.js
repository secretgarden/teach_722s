TAG_GAME_T1 = 111;
TAG_GAME_T2 = 112;
TAG_GAME_T3 = 113;
TAG_GAME_T4 = 114;
TAG_GAME_T5 = 115;
TAG_GAME_T6 = 116;
TAG_GAME_T7 = 117;
TAG_GAME_T8 = 118;
TAG_GAME_T9 = 119;

TAG_GAME_I1 = 511;
TAG_GAME_I2 = 512;
TAG_GAME_I3 = 513;
TAG_GAME_I4 = 514;
TAG_GAME_I5 = 515;
TAG_GAME_I6 = 516;
TAG_GAME_I7 = 517;
TAG_GAME_I8 = 518;
TAG_GAME_I9 = 519;

TAG_GAME_V1 = 11;
TAG_GAME_V2 = 12;
TAG_GAME_V3 = 13;
TAG_GAME_V4 = 14;
TAG_GAME_V5 = 15;
TAG_GAME_V6 = 16;

var GameMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
	curSel:0,
	textArr: null,
	imageArr : null,
	tagArr1: null,
	tagArr2: null,
	tagVal: null,
	angelArr: [],
    ctor:function () {
        this._super();
        this.init();
        gg.game = this;
    },
    init:function(){
    	this.loadData();
    	this.loadAngel();
    	this.loadPosition();
    	this.loadBack();
    },
    loadData:function(){
    	this.textArr = ["#game_biseguan.png","#game_biseming.png","#game_cajingzhi.png",
    	                "#game_xishuizhi.png","#game_yiyeguan.png","#game_zhehuangti.png",
    	                "#game_xishuizhi.png","#game_yiyeguan.png","#game_zhehuangti.png"];// ,"吸水纸","擦镜纸"
    	this.imageArr = ["#img_biseguan.png","#img_biseming.png","#img_cajingzhi.png",
    	                 "#img_xishuizhi.png","#img_yiyeguan.png","#img_zheguangti.png",
    	                 "#img_xishuizhi.png","#img_yiyeguan.png","#img_zheguangti.png"];
    	this.tagArr1 = [TAG_GAME_T1,TAG_GAME_T2,TAG_GAME_T3,TAG_GAME_T4,TAG_GAME_T5,TAG_GAME_T6,TAG_GAME_T7,TAG_GAME_T8,TAG_GAME_T9];
    	this.tagArr2 = [TAG_GAME_I1,TAG_GAME_I2,TAG_GAME_I3,TAG_GAME_I4,TAG_GAME_I5,TAG_GAME_I6,TAG_GAME_I7,TAG_GAME_I8,TAG_GAME_I9];
    	this.tagVal  = [TAG_GAME_V1,TAG_GAME_V2,TAG_GAME_V3,TAG_GAME_V4,TAG_GAME_V5,TAG_GAME_V6,TAG_GAME_V4,TAG_GAME_V5,TAG_GAME_V6];
    },
    loadAngel:function(){
    	this.angelArr = [];
    	for(var i = 0; i < this.textArr.length; i++){
    		var tt = this.textArr[i];
    		var text = new Angel(this,tt,this.callback,this);
    		text.setTag(this.tagArr1[i]);
    		text.value = this.tagVal[i];
    		this.angelArr.push(text);
    	}
    	for(var i = 0; i < this.imageArr.length; i++){
    		var img = this.imageArr[i];
    		var image = new Angel(this,img,this.callback,this);
    		image.setTag(this.tagArr2[i]);
    		image.value = this.tagVal[i];
    		this.angelArr.push(image);
    	}
    },
    loadPosition:function(){
    	this.angelArr.sort($.randomSort);
    	var x= 163,y = 457, marign = 0,cur = 0;
    	for(var i in this.angelArr){
    		var angel = this.angelArr[i];
    		if(angel.getTag() / 100 >= 5){
    			continue;
    		}
    		// 字
    		angel.setPosition(x + marign,y);
    		cc.log(x+"|||"+y);
    		x += angel.width * 2;
    		if(cur%3 == 2){
    			if(marign == 0){
    				marign = angel.width;
    			} else {
    				marign = 0;
    			}
    			x = 163;
    			y -= angel.height;
    		}
    		cur ++;
    	}
    	x = 163,y = 457, marign = -1;
    	for(var i in this.angelArr){
    		var angel = this.angelArr[i];
    		if(angel.getTag() / 100 < 5){
    			continue;
    		}
    		// 图
    		if(marign == -1){
    			marign = angel.width;
    		}
    		angel.setPosition(x + marign,y);
    		cc.log(x+"|||"+y);
    		x += angel.width * 2;
    		if(cur%3 == 2){
    			if(marign == 0){
    				marign = angel.width;
    			} else {
    				marign = 0;
    			}
    			x = 163;
    			y -= angel.height;
    		}
    		cur ++;
    	}
    },
    callback:function(pSend){
    	if(pSend == this.restart){
    		cc.log("重新开始");
    		_.stop();
    		$.runScene(new StartScene());
    		return;
    	}
    	cc.log("say:" + pSend.getTag());
    	if(pSend.getTag() == this.curSel){
    		return;
    	}
    	var cur = this.getChildByTag(this.curSel);
    	var tag = pSend.getTag();
    	this.checkAndClear(pSend,cur);
    	this.checkAndOver();
    },
    checkAndClear:function(pSend,cur){
    	if(this.curSel != 0){
    		if(pSend.value == cur.value && 
    			Math.floor(pSend.getTag()/100)!=Math.floor(cur.getTag()/100)){
    			var pos1 = cur.getPosition();
    			var pos2 = pSend.getPosition();
    			cur.removeFromParent(true);
    			pSend.removeFromParent(true);
    			var duang1 = new Duang(this, pos1.x, pos1.y, res_game.spark);
    			duang1.destory();
    			var duang2 = new Duang(this, pos2.x, pos2.y, res_game.spark);
    			duang2.destory();
    			this.curSel = 0;
    			_.gameBroken();
    			return;
    		} else {
    			cur.stop2();	
    		}
    	} 
    	this.curSel = pSend.getTag();
		pSend.flash2();
    },
    checkAndOver: function (){
    	for(var i in this.tagArr1){
    		var tag = this.tagArr1[i];
    		var tt =this.getChildByTag(tag);
    		if(tt != null){
    			return;
    		}
    	}
    	this.getParent().loadResultLayer();
    },
	loadBack:function(){
		var back = new Angel(this,"#game_back.png",function(){
			$.runScene(new StartScene());
		},this);
		back.setPosition(10 + back.width * 0.5, 10 + back.height * 0.5);
	}
});

