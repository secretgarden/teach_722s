var FlashTip = cc.LabelBMFont.extend({
	ctor:function (parent, str, fntFile) {
		this._super(str, fntFile);
		this.setColor(cc.color(0,0,0));
	},
	doFlash:function(str){
		this.setString(str);
		this.scheduleOnce(function(){
			this.setString("");	
		}.bind(this), 1.5);
	}
});