var Button = Angel.extend({
	margin: 0,
	menu:null,
	flash_name:null,
	ctor: function (parent,zIndex,tag,normalImage, callback, back) {
		this._super(parent,normalImage, callback, back);
		this.setTag(tag);
		this.setLocalZOrder(zIndex);
		// 默认不能点击
		this.setEnable(false);
	},
	preCall:function(){
		// 隐藏箭头
		this.setEnable(false);
		this.stop();
		// 操作成功
		ll.tip.mdScore(10);
		if(!gg.errFlag){
			gg.oneSure ++;
		}
		gg.errFlag = false;
		_.clever();
	},
	exeUnEnable:function(){
		// 操作失败
		ll.tip.mdScore(-3);
		gg.errFlag = true;
		gg.errorStep ++;
		_.error();
	},
	kill:function(){
		this.removeFromParent(true);
	},
	hiddleAndKill:/**
	 * @param dt
	 *            时间,默认0.5秒
	 */
		function(dt){
		if(dt == null){
			dt = 0.5;
		}
		this.runAction(cc.fadeOut(dt),cc.callFunc(function(){
			this.kill();
		}, this));
	}
})