/**
 * 按钮基类
 */
Angel = cc.Sprite.extend({
	p:null,
	callback:null,
	back:null,
	enable:null,
	normalName:null,
	hoverName:null,
	flashName:null,
	ctor:function (parent, fileName, callback, back, rect, rotated) {
		if(gg.isdemo){
			this.enable = false;
		} else {
			this.enable = true;
		}
		this._super(fileName, rect, rotated);
		this.p = parent;
		this.normalName = fileName.split("#")[1];
		this.hoverName = fileName.split("#")[1];
		this.flashName = "red/" + fileName.split("#")[1];
		this.orig_color = this.getColor();
		this.callback = callback;
		if(back == null){
			this.back = this.p;
		} else {
			this.back = back;
		}
		this.init();
	},
	init:function(){
		// 添加到界面
		this.p.addChild(this);
		// 添加按钮事件
		this.listener();
	},
	genFlashName:function (){
		this.file_name = this.file_name.split("#")[1];
		this.flash_name = "red/" + this.file_name; 
	},
	checkClick:function(pos){
		// 判断触碰 getBoundingBoxToWorld getBoundingBox
		if(cc.rectContainsPoint(
				this.getBoundingBoxToWorld(),pos)){
			return true;
		} else {
			return false;
		}
	},
	showDesc:function(tag){
		if(gg.cur_tool == TAG_BUTTON_HELP){
			for(var i = 0; i< sprite_json.length; i++){
				if(sprite_json[i].tag == tag){
					if(gg.tip_layer!=null){
						gg.tip_layer.sayFlash(sprite_json[i].desc);	
					}
					cc.log("提示||"+sprite_json[i].desc);
					break;
				}
			}
		}
	},
	onTouchBegan:function(touch, event){
		if(gg.synch_l){
			return false;
		}
		if(!this.enable){
			return false;
		}
		var target = event.getCurrentTarget();
		if(!target.checkVisible()){
			return false;
		}
		var pos = cc.p(touch.getLocationX(),touch.getLocationY());
		// 被点击则，更换点击图片
		if(target.checkClick(pos)){
			this.hover = true;
			target.setSpriteFrame(target.hoverName);
			if(this.checkPre(target, this)){
				// 如果触发事件，同步
				gg.synch_l = true;
				cc.log("gg.synch_l:" + gg.synch_l + "|tag:"+target.getTag())
			}
		}
		return true;
	},
	onTouchEnded:function(touch, event){
		var target = event.getCurrentTarget();
		if(this.hover){
			// 点击结束，更换常态图片
			this.hover = false;
			gg.synch_l = false;
			cc.log("gg.synch_l:" + gg.synch_l + "|tag:"+target.getTag())
			target.setSpriteFrame(target.normalName);
			if(!this.checkBack(target, this)){
				return;
			}
			if(target.callback != null){
				// 有回调函数，则调用回调函数
				target.callback.call(target.back,target);
			}
		}
	},
	simCallBack:function(){
		if(this.callback != null){
			this.callback.call(this.back,this);	
		}
	},
	checkVisible:function(){
		if(!this.isVisible()){
			return false;
		}
		if(!this.getParent().isVisible()){
			return false;
		}
		return true;
	},
	checkBack:function(target, listener){
		// 回调检查
		return true;
	},
	checkPre:function(target, listener){
		// 回调预检查
		return true;
	},
	setSFrame:function(sFrame){
		this.setSpriteFrame(sFrame);
		this.normalName = sFrame;
		this.hoverName = sFrame;
		this.flashName = "red/" + sFrame;
	},
	listener:function(){
		var listener_touch = cc.EventListener.create({
			event: cc.EventListener.TOUCH_ONE_BY_ONE,
			swallowTouches: false,
			hover: false,
			onTouchBegan:this.onTouchBegan.bind(this),
			onTouchEnded:this.onTouchEnded.bind(this)});
		cc.eventManager.addListener(listener_touch, this);
	},
	setHoverName:function(fileName){
		this.hoverName = fileName;
	},
	flash_flag:false,
	flash:function(){
		if(gg.teach_type != TAG_LEAD){
			return;
		}
		// 是否在闪现
		this.flash_flag = true;
		this.schedule(this.updateFlash, 0.4);
	},
	file_name_flag:true,
	updateFlash:function(){
		if(this.file_name_flag){
			this.file_name_flag = false;
			var frame1 = cc.spriteFrameCache.getSpriteFrame(this.normalName);
			this.setSpriteFrame(frame1);	
		} else {
			this.file_name_flag = true; 
			var frame2 = cc.spriteFrameCache.getSpriteFrame(this.flashName);
			this.setSpriteFrame(frame2);
		}
	},
	stop:function(){
		this.unschedule(this.updateFlash);
		if(this.flash_flag){
			this.setSpriteFrame(this.normalName);	
		}
	},
	setEnable:function(enable){
		this.enable = enable;
	},
	flash2:function (){
		var fade1 = cc.fadeTo(0.5, 50);
		var fade2 = cc.fadeTo(0.5, 255);
		var seq = cc.sequence(fade1,fade2);
		var tint1 = cc.tintTo(0.5,170,230,255);
		var tint2 = cc.tintTo(0.5,255,255,255);
		var seq2 = cc.sequence(tint1,tint2);
		var spawn = cc.spawn(seq, seq2);
		var flash = cc.repeatForever(spawn);
		this.runAction(flash);
	},
	stop2:function (){
		this.stopAllActions();
		this.setOpacity(255);
		this.setColor(this.orig_color);
	},
	up:function (standard, margin){
		if(margin != null){
			this.margin = margin;
		}
		this.setPosition(standard.x, standard.y + standard.height + this.margin);
	},
	down:function (standard, margin){
		if(margin != null){
			this.margin = margin;
		}
		this.setPosition(standard.x, standard.y - this.height - this.margin);
	},
	left:function (standard, margin){
		if(margin != null){
			this.margin = margin;
		}
		this.setPosition(standard.x - this.width - this.margin, standard.y);
	},
	right:function (standard, margin){
		if(margin != null){
			this.margin = margin;
		}
		this.setPosition(standard.x + standard.width + this.margin, standard.y);
	}
})