var Button = Angel.extend({
	margin: 0,
	menu:null,
	flash_name:null,
	ctor: function (parent,zIndex,tag,normalImage, callback, back) {
		this._super(parent,normalImage, callback, back);
		this.setTag(tag);
		this.setLocalZOrder(zIndex);
	},
	/*-
	flash:function (){
		if(gg.teach_type != TAG_LEAD){
			return;
		}
		var fade1 = cc.fadeTo(0.5, 50);
		var fade2 = cc.fadeTo(0.5, 255);
		var seq = cc.sequence(fade1,fade2);
		var tint1 = cc.tintTo(0.5,170,230,255);
		var tint2 = cc.tintTo(0.5,255,255,255);
		var seq2 = cc.sequence(tint1,tint2);
		var spawn = cc.spawn(seq, seq2);
		var flash = cc.repeatForever(spawn);
		this.runAction(flash);
	},*/
	checkPre:function(target, listener){
		if(gg.flow != null){
			if(!gg.synch_listener){
				// 同步锁，同步
				return false;
			}
			// 提示，同步
			if(gg.cur_tool == TAG_BUTTON_HELP){
				return true;
			}
			if(gg.flow.checkTag(target.getTag())){
				// 找到点击事件，同步
				return true;
			} else {
				return false;
			}
		} else {
			// 没有流，普通按钮，同步
			return true;
		}
	},
	checkBack:function (target, listener){
		if(gg.flow != null){
			if(!gg.synch_listener){
				// 同步锁，不回调
				return false;
			}
			// 显示描述
			target.showDesc(target.getTag());
			// 不是操作状态，不回调
			if(gg.cur_tool != TAG_BUTTON_HAND){
				return false;
			}
			if(!gg.flow.checkTag(target.getTag())){
				// 没有轮到点击事件，不回调
				if(target.getTag() == TAG_FACE_POLE){
					// TODO 特例
				} else {
					_.error();
					gg.score -= 3;
					if(gg.score<0){
						gg.score=0;
					}
					gg.errFlag = true;
					gg.errorStep ++;
				}
				return false;
			} else {
				_.clever();
				gg.score += 10;
				if(!gg.errFlag){
					gg.oneSure ++;
				}
				gg.errFlag = false;
				// 点击事件，触摸吞噬
				return true;
			}
		} else {
			return false;
		}
	}
})