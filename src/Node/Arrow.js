Arrow = cc.Sprite.extend({
	help:null,
	scene:null,
	ctor:function (scene, fileName, rect, rotated) {
		this._super(fileName, rect, rotated);
		this.scene = scene;
		this.init();
	},
	init:function (){
		var animFrames = [];
		for (var i = 1; i < 4; i++) {
			var str = "button/arr" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation = new cc.Animation(animFrames, 0.15);
		var arringAction = cc.RepeatForever.create(cc.Animate.create(animation));
		this.runAction(arringAction);
		
		var arringAction2 = cc.RepeatForever.create(cc.Animate.create(animation));
	},
	pos:function(obj,x,y){
		x = x == null ? 1 : x;
		y = y == null ? 1 : y;
		var p = cc.p(obj.x + obj.width * x, obj.y + obj.height * y);
		if(gg.teach_type == TAG_REAL){
			this.setVisible(false);
			return;
		}
		this.setPosition(p);
		this.setVisible(true);
	}
});