PScene = cc.Scene.extend({
	onEnter:function () {
		this._super();
		this.preLoad();
	},
	preLoad:function(){
		if (cc.sys.platform == cc.sys.DESKTOP_BROWSER) {
			// html5 浏览器
			cc.log("html5");
		} else{
			// cc.sys.os native 不需要预加载资源
			gg.run_load = true;
			gg.finish_load = true;
			gg.game_load = true;
			cc.log("jsb");
		}
		
		if(!gg.run_load){
			cc.loader.load(g_resources_run,
					function () {
				gg.run_load = true;
				cc.log("游戏页面加载完成");
			});
		}
		if(!gg.finish_load){
			cc.loader.load(g_resources_finish, 
					function () {
				gg.finish_load = true;
				cc.log("结束页面加载完成");
			});
		}
		if(!gg.game_load){
			cc.loader.load(g_resources_game, 
					function () {
				gg.game_load = true;
				cc.log("小游戏页面加载完成");
			});
		}
		if(!gg.data_load){
			cc.loader.load(g_resources_data, 
					function () {
				gg.data_load = true;
				cc.log("数据处理页面加载完成");
			});
		}
	}
});