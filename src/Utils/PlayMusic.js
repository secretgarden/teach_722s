var PlayMusic = Music.extend({
	bg_flag:false,
	ctor:function(){
		this.bg_flag = false;
		this.effFlag = false;
	},
	clever:function(){
		this.playEffect(res_run.ok_mp3)
	},
	error:function(){
		this.playEffect(res_run.error_mp3)
	},
	playBg:function(){
		this.playMusic(res_start.bg_mp3);
	},
	cPause:function(){
		this.ban();
	},
	cResume:function(){
		this.lift();
	},
	isStop:function(){
		return !this.effFlag;
	},
	clock:function(){
		this.playEffect(res_run.clock_mp3, true);
	},
	stopClock:function(){
		cc.audioEngine.stopAllEffects();
	},
	over:function(){
//		this.playEffect(res_finish.over_mp3, true);
	},
	gameBroken:function(){
		if(this.eff_flag){
			cc.audioEngine.playEffect(res_game.broken, false);
		}
	},
	gameWin:function(){
		if(this.eff_flag){
//			cc.audioEngine.playEffect(res_game.win, false);
		}
	}
});