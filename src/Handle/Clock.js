Clock = cc.Sprite.extend({
	point:null,
	ctor:function (parent,fileName, rect, rotated) {
		this._super(fileName, rect, rotated);
		parent.addChild(this, 1000);
		this.init();
	},
	init:function () {
		this.setPosition(gg.width * 0.5, gg.height * 0.7);
		this.setVisible(false);
		this.point = new cc.Sprite("#other/point.png");
		this.point.setPosition(this.width * 0.5, this.height * 0.5);
		this.addChild(this.point);
		this.action = cc.repeatForever(cc.rotateBy(2,360));
		this.action.retain();
	},
	doing:function () {
		this.setVisible(true);
		_.clock();
		this.point.runAction(this.action);
	},
	stop:function () {
		this.setVisible(false);
		_.stopClock();
		this.point.stopAllActions();
	}
})