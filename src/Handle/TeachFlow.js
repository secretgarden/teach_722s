var TeachFlow = cc.Class.extend({
	step:0,
	flow:null,
	main:null,
	tip:null,
	tip_frame:null,
	arr:null,
	flash:null,
	over_flag: false,
	curSprite:null,
	ctor: function(){
	},
	setMain:function(main){
		this.main=main;
	},
	init: function(tip, tip_frame, flash, arr){
		this.tip = tip;
		this.arr = arr;
		this.flash = flash;
		for(var i in teach_flow){
			teach_flow[i].finish = false;
			teach_flow[i].cur = false;
			teach_flow[i].id = i - -1;
			if(teach_flow[i].action == null){
				teach_flow[i].action = ACTION_NONE;				
			}
			if(teach_flow[i].lock == null){
				teach_flow[i].lock = true;
			}
		}
		this.tip_frame = tip_frame;
	},
	start:function(){
		this.over_flag = false;
		this.step = 0;
		this.tip_frame.init(teach_flow);
	},
	over:function(){
		this.over_flag = true;
		this.flow = over;
		gg.lastStep = this.step;
		this.main.over();
	},
	tipColorBack:function(){
		this.flow.cur=false;
	},
	checkTag:function(tag){
		var cur_flow = teach_flow[this.step - 1];
		if(cur_flow.tag == tag){
			return true;
		} else {
			return false;
		}
	},
	prev:function(count){
		if(this.curSprite!=null){
			this.curSprite.stop();
			this.curSprite = null;
		}
		if(this.flow!=null){
			this.flow.cur = false;
		}
		this.step = this.step - count;
		this.flow = teach_flow[this.step - 1];
		this.refresh();
		ll.tip.mdScore(-3);
	},
	next:function(){
		if(this.over_flag){
			return;
		}
		if(this.curSprite!=null){
			this.curSprite.stop();
			this.curSprite = null;
		}
		if(this.flow!=null){
			this.flow.cur = false;
			// 标记任务已完成
			this.flow.finish = true;
		}
		this.flow = teach_flow[this.step++];
		if(this.flow.finish){
			// 如果任务已完成，跳过当前步骤
			this.next();
		}
		this.refresh();
		if(this.curSprite!=null){
			this.curSprite.setEnable(true);
		}
		this.doDemo(this.curSprite);
	},
	doDemo:function(curSprite){
		if(gg.isdemo){
			// 演示
			var delay1 = 2;
			var delay2 = 1;
			if(curSprite != null){
				cc.log("demo1:当前步骤"+this.step + "|tag:" + curSprite.getTag() +"|名称:"+this.getDesc(curSprite.getTag()));
				curSprite.doFlag = false;
				curSprite.scheduleOnce(function(){
					cc.log("demo2:" + curSprite.doFlag);
					if(curSprite.doFlag){
						return;		
					} else {
						curSprite.doFlag = true;
					}
					var rect = this.getBoundingBoxToWorld();
					var relPos = new cc.p(rect.x + rect.width * 0.5, rect.y + rect.height * 0.5);
					gg.arr.setPosition(relPos);
					
					this.scheduleOnce(function(){
						gg.arr.setPosition(-100, -100);
						cc.log("demo3:当前步骤"+this.step + "|tag:" + this.getTag())
						if(this.simCallBack !=null){
							this.simCallBack();	
						} else {
							this.activate();
						}
					}.bind(this),delay2);
				}.bind(curSprite), delay1);
			}
		}
	},
	getDesc:function(tag){
		for(var i = 0; i< sprite_json.length; i++){
			if(sprite_json[i].tag == tag){
				return sprite_json[i].desc;
			}
		}

	},
	refresh:function(){
		// 刷新提示
		this.flow.cur = true;
		if(this.flow.tip != null){
			this.tip.doTip(this.flow.id + "." + this.flow.tip);
		}
		if(this.flow.flash != null){
			this.flash.doFlash(this.flow.flash);
		}
		if(this.flow.arr_tip != null){
			this.arr.setVisible(true);
			this.exeArr(this.flow.arr_tip,this.flow.arr_tip_x,this.flow.arr_tip_y);
		} else {
			this.arr.setVisible(false);
		}
		if(this.step > teach_flow.length - 1){
			this.over();
		}
		this.initCurSprite();
		if(this.curSprite!=null){
			this.curSprite.flash();	
		}
		
		var before = teach_flow[this.step - 2];
		if(before == null || before.lock){
			gg.synch_listener = true;	
		}
	},
	getStep:function(){
		return this.step;
	},
	initCurSprite:function(){
		var tag = this.flow.tag;
		if(tag == null || tag == undefined){
			return;
		}
		var layer = this.main.run_layer;
		var sprite = layer.getChildByTag(tag);
		if(sprite != null){
			this.curSprite = sprite;
			return;
		}
		tagArr = [TAG_722S_BACK_WIN,
		          TAG_722S_FACE_WIN,
		          TAG_722S_SCREEN_WIN];
		for(var i = 0; i < tagArr.length; i++){
			var at = tagArr[i];
			var root = layer.getChildByTag(at);
			if(root == null){
				continue;
			}
			sprite = root.getChildByTag(tag);
			if(sprite != null){
				this.curSprite = sprite;
				return;
			}
		}
	},
	exeArr:function(tag,x,y){
		var obj = this.main.run_layer.runSpriteSheet.getChildByTag(tag);
		if(obj == null){
			return;
		}
		this.arr.pos(obj,x,y);
	}
});
// 任务流
teach_flow = [
	{tip:"来到仪器后面",tag:TAG_ITEM_BODY,action:ACTION_DO1},
	{tip:"打开电源开关",tag:TAG_BUTTON_START},
    {tip:"靠近仪器",tag:TAG_ITEM_BODY,action:ACTION_DO2},
    {tip:"按动“功能键”，切换至透射比测试模式",tag:TAG_BUTTON_FUN,action:ACTION_DO1},
    {tip:"转动“旋钮”，刻度调整到510"},
    {tip:"打开样品室盖子",tag:TAG_722S_FACE,action:ACTION_DO1},
    {tip:"放入遮光体",tag:TAG_BUTTON_ZHEG},
    {tip:"合上仪器盖子",tag:TAG_722S_FACE,action:ACTION_DO2},
    {tip:"按“调0%”键",tag:TAG_BUTTON_DOWN},
    {tip:"打开样品室盖子",tag:TAG_722S_FACE,action:ACTION_DO1},
	{tip:"取出遮光体",tag:TAG_722S_FACE,action:ACTION_DO3},
	{tip:"使用比色皿",tag:TAG_BUTTON_BISE,action:ACTION_DO1},
	{tip:"用蒸馏水润洗三次"},
	{tip:"用参比溶液润洗三次"},//
	{tip:"加参比溶液到比色皿的4/5处"},// ,tag:TAG_ITEM_WATER,action:ACTION_DO1
	{tip:"用吸水纸，轻触比色皿",tag:TAG_BUTTON_XISHUI},
	{tip:"用擦镜纸，擦拭比色皿",tag:TAG_BUTTON_CHAJING},
	{tip:"放入比色皿",tag:TAG_BUTTON_BISE,action:ACTION_DO2},
	{tip:"合上仪器盖子",tag:TAG_722S_FACE,action:ACTION_DO4},
	{tip:"按动“功能键”，切换至吸光度测试模式",tag:TAG_BUTTON_FUN,action:ACTION_DO2},
	{tip:"按动“功能键”，切换至吸光度测试模式",tag:TAG_BUTTON_FUN,action:ACTION_DO2},
	{tip:"按动“功能键”，切换至吸光度测试模式",tag:TAG_BUTTON_FUN,action:ACTION_DO2},
	{tip:"按“调100%”键",tag:TAG_BUTTON_UP},
	{tip:"显示“BL”"},
	{tip:"显示“0000”"},
	{tip:"使用蒸馏水润洗三次"},
	{tip:"使用待测溶液润洗三次"},// ,tag:TAG_ITEM_WATER,action:ACTION_DO1
	{tip:"添加待测溶液到比色皿的4/5处"},// ,tag:TAG_ITEM_WATER,action:ACTION_DO1
	
	{tip:"打开样品室盖子",tag:TAG_722S_FACE,action:ACTION_DO1},
	{tip:"放入比色皿",tag:TAG_BUTTON_BISE,action:ACTION_DO3},
	{tip:"合上仪器盖子",tag:TAG_722S_FACE,action:ACTION_DO5},
	{tip:"拉动杆子，使遮光体对准光路",tag:TAG_FACE_POLE,action:ACTION_DO1},
	{tip:"显示“0.235”"},
	
	{tip:"打开样品室盖子",tag:TAG_722S_FACE,action:ACTION_DO1},
	{tip:"放入比色皿",tag:TAG_BUTTON_BISE,action:ACTION_DO4},
	{tip:"合上仪器盖子",tag:TAG_722S_FACE,action:ACTION_DO5},
	{tip:"拉动杆子，使遮光体对准光路",tag:TAG_FACE_POLE,action:ACTION_DO2},
	{tip:"显示“0.234”"},
	
	{tip:"打开样品室盖子",tag:TAG_722S_FACE,action:ACTION_DO1},
	{tip:"放入比色皿",tag:TAG_BUTTON_BISE,action:ACTION_DO5},
	{tip:"合上仪器盖子",tag:TAG_722S_FACE,action:ACTION_DO5},
	{tip:"拉动杆子，使遮光体对准光路",tag:TAG_FACE_POLE,action:ACTION_DO3},
	{tip:"显示“0.237”"},
	
	{tip:"来到仪器后面",tag:TAG_ITEM_BODY,action:ACTION_DO1},
	{tip:"关闭电源开关",tag:TAG_BUTTON_START},
	{tip:"恭喜过关",over:true}
];
over = {tip:"恭喜过关"};



