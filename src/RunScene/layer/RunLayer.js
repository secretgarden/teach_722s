var RunLayer = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.clock = new Clock(this,"#other/clock.png");
		this.arr = new Arrow(this.getParent(),"#button/arr0.png");
		this.addChild(this.arr, 10);
		
		var deskItem = new Button(this,
				d_z_index, TAG_ITEM_DESK,
				"#desk.png",
				this.callback,this);
		deskItem.setPosition(512, 230);
		var deskY = deskItem.y + deskItem.height * 0.5 - 40;
		this.deskY = deskY;
		
		var bodyItem = new Button(this,
				10, TAG_ITEM_BODY,
				"#722s_desk.png",
				this.callback,this);
		bodyItem.setPosition(420, 370);
		// 遮光体
		var zheg = new Button(this,10,TAG_ITEM_ZHEG,"#zheguangti.png",this.callback,this);
		zheg.setPosition(620,350);
		// 比色皿
		var bise1 = new Button(this,10,TAG_ITEM_BISE1,"#bise.png",this.callback,this)
		bise1.setPosition(650,350);
		// 蒸馏水
		var water = new Button(this,10,TAG_ITEM_WATER,"#water.png",this.callback,this)
		water.setPosition(700,375);
		
		this.back = new SideWin(this,TAG_722S_BACK_WIN);
		this.face = new FaceWin(this,TAG_722S_FACE_WIN);
		this.screen = new ScreenWin(this,TAG_722S_SCREEN_WIN);
		
		this.flashCloud = new FlashCloud(this,TAG_722S_CLOUD2);
		this.note = new NoteWin(this, res_run.note_bg, 100);
		
		this.useTool();
	},
	useTool:function() {
		var default_index = 100;
		var zhegTool = new Button(this,default_index,TAG_BUTTON_ZHEG,"#button/button_zheguangti.png",this.toolBack,this);
		zhegTool.setPosition(550,630);
		var zhegDesc = new cc.LabelTTF("遮光体",gg.fontName,20);
		zhegDesc.setColor(cc.color(0,0,0));
		zhegDesc.setPosition(550, 585);
		this.addChild(zhegDesc);
		
		var biseTool = new Button(this,default_index,TAG_BUTTON_BISE,"#button/button_bise.png",this.toolBack,this);
		biseTool.setPosition(620,630);
		var biseDesc = new cc.LabelTTF("比色皿",gg.fontName,20);
		biseDesc.setColor(cc.color(0,0,0));
		biseDesc.setPosition(620, 585);
		this.addChild(biseDesc);
		
		var xishuiTool = new Button(this,default_index,TAG_BUTTON_XISHUI,"#button/button_xishui.png",this.toolBack,this);
		xishuiTool.setPosition(690,630);
		var xishuiDesc = new cc.LabelTTF("吸水纸",gg.fontName,20);
		xishuiDesc.setColor(cc.color(0,0,0));
		xishuiDesc.setPosition(690, 585);
		this.addChild(xishuiDesc);
		
		var chajingTool = new Button(this,default_index,TAG_BUTTON_CHAJING,"#button/button_cajing.png",this.toolBack,this);
		chajingTool.setPosition(760,630);
		var chajingDesc = new cc.LabelTTF("擦镜纸",gg.fontName,20);
		chajingDesc.setColor(cc.color(0,0,0));
		chajingDesc.setPosition(760, 585);
		this.addChild(chajingDesc);
		
		var noteButton = new Angel(this,"#sel1.png",this.toolBack,this);
		noteButton.setHoverName("sel2.png");
		noteButton.setTag(TAG_BUTTON_NOTE);
		noteButton.setPosition(830,630);
		noteButton.setVisible(false);
	},
	toolBack:function(pSender){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.toolDone, this);
		gg.synch_listener = false;
		switch (pSender.getTag()){
		case TAG_BUTTON_ZHEG:
			// 放入遮光体
			var zheg = this.getChildByTag(TAG_ITEM_ZHEG);
			zheg.setPosition(425,300);
			zheg.setLocalZOrder(200);
			zheg.setVisible(false);
			this.c7 = new C7Cloud(this,TAG_722S_CLOUD7);
			this.cloud = new C6Cloud(this,TAG_722S_CLOUD);
			break;
		case TAG_BUTTON_BISE:
			if(action == ACTION_DO1){
				this.cloud = new C2Cloud(this,TAG_722S_CLOUD);
				gg.flow.next();
// this.flashCloud.doFlash("1、手要拿毛面，不能拿光面；\n2、溶液倒入应当在2/3~4/5");
			} else {
				var targetPos = cc.p(500,370);
				if(action == ACTION_DO2){
					targetPos = cc.p(500,370);
				} else if(action == ACTION_DO3){
					targetPos = cc.p(500,390);
				} else if(action == ACTION_DO4){
					targetPos = cc.p(500,410);
				} else if(action == ACTION_DO5){
					targetPos = cc.p(500,430);
				}
				this.c7.open();
				var bise1 = this.getChildByTag(TAG_ITEM_BISE1);
				bise1.setVisible(true);
				bise1.setLocalZOrder(200);
				bise1.setPosition(pSender.getPosition());
				var move = cc.moveTo(1, targetPos);
				var seq = cc.sequence(move,func);
				bise1.runAction(seq);
			}
			break;
		case TAG_BUTTON_XISHUI:
			var xishui = new cc.Sprite("#xishuizhi.png");
			xishui.setPosition(pSender.getPosition());
			this.addChild(xishui,100,TAG_SPRITE_XISHUI);
			var move = cc.moveTo(1, cc.p(422,326));
			var seq = cc.sequence(move,func);
			xishui.runAction(seq);
			break;
		case TAG_BUTTON_CHAJING:
			var cajing = new cc.Sprite("#cajinzhi.png");
			cajing.setPosition(pSender.getPosition());
			this.addChild(cajing,100,TAG_SPRITE_CHAJING);
			var move = cc.moveTo(1, cc.p(422,306));
			var move2 = cc.moveTo(0.3, cc.p(422,326));
			var move3 = cc.moveTo(0.3, cc.p(422,296));
			var seq = cc.sequence(move,move2,move3,func);
			cajing.runAction(seq);
			break;
		case TAG_BUTTON_NOTE:
			if(this.note.isOpen()){
				this.note.close();
			} else {
				this.note.open();
			}
			break;
		}
	},
	toolDone:function(pSender){
		switch(pSender.getTag()){
			case TAG_ITEM_ZHEG:
			case TAG_ITEM_BISE1:
				pSender.setVisible(false);
				var action = gg.flow.flow.action;
				if(action == ACTION_DO2){
					this.c7.putBise1();
				} else if(action == ACTION_DO3){
					this.c7.putBise2();
				} else if(action == ACTION_DO4){
					this.c7.putBise3();
				} else if(action == ACTION_DO5){
					this.c7.putBise4();
				} else {
					gg.flow.next();
				}
				break;
			case TAG_SPRITE_XISHUI:
				pSender.setSpriteFrame("bise9.png");
				this.scheduleOnce(function(){
					pSender.removeFromParent(true);
					gg.flow.next();
				}.bind(this),1);
				break;
			case TAG_SPRITE_CHAJING:
				pSender.removeFromParent(true);
				this.closeCloud();
				gg.flow.next();
				break;
		}
	},
	closeCloud:function(){
		if(this.cloud != null){
			this.cloud.close();
		}
	},
	callback:function (pSender){
		cc.log("say:" + pSender.getTag());
		var action = gg.flow.flow.action;
		gg.synch_listener = false;
		switch (pSender.getTag()){
			case TAG_ITEM_BODY:
				if(action == ACTION_DO1){
					this.back.open();
				} else if(action == ACTION_DO2){
					this.face.open();
					this.screen.open();
				}
				gg.flow.next();
				break;
			case TAG_ITEM_DESK:
				gg.flow.next();
				break;
			case TAG_ITEM_BISE1:
				gg.flow.next();
				break;
		}
	}
});