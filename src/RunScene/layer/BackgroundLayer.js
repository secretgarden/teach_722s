var RunBackgroundLayer = cc.Layer.extend({
    ctor:function () {
        this._super();
        this.loadBg();
        this.loadLogo();
        return true;
    },
    loadBg : function(){
    	var node = new cc.Sprite("#bg.png");
        this.addChild(node);
        node.setPosition(gg.c_p);
    },
    loadLogo : function(){
    	this.logoItem = new cc.MenuItemImage(res_run.logo,res_run.logo,this.eventMenuCallback,this);
    	this.logoItem.setPosition(this.logoItem.width * 0.5 + 10, height - this.logoItem.height * 0.5 - 10);

    	this.menu = new cc.Menu();
    	this.menu.addChild(this.logoItem, 1, TAG_LOGO);
    	this.menu.setPosition(0, 0);
    	this.addChild(this.menu, 10);
    },
    eventMenuCallback: function(pSender) {
    	if(!gg.logo_onclick){
    		return;
    	}
    	switch (pSender.getTag()){
    	case TAG_LOGO:
    		cc.log("进入官网");
    		location.href = "http://www.zczyedu.net/index.aspx";
    		break;
    	default:
    		break;
    	}
    }
});