var RunMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
    ctor:function () {
        this._super();
        this.loadTip();
        this.loadRun();
        this.loadTool();
        this.loadFlow();
// this.loadTest();
        return true;
    },
    loadTest:function(){
	  	for(var i=0; i<10; i++){
	  		gg.flow.next();
	  	}
    },
    loadTip:function(){
    	this.tip_layer = new TipLayer(this);
    	ll.tip = this.tip_layer; 
    },
    loadTool:function(){
    	this.tool_layer = new ToolLayer(this);
    },
    loadRun:function(){
    	this.run_layer = new RunLayer(this);
    },
    loadFlow:function(){
    	gg.flow.setMain(this);
    	gg.flow.init(this.tip_layer.tip,this.tip_layer.tip_frame, this.tip_layer.flash, this.run_layer.arr);
    	gg.flow.start();
    	gg.flow.next();
    },
    over: function (){
    	this.tip_layer.over();
    	this.scheduleOnce(function(){
    		$.runScene(new FinishScene());
    	},2);
    	// 提交成绩
    	net.saveScore();
    },
});
