var Window = cc.Class.extend({
	scene: null,
	layer: null,
	tag: null,
	rootNode:null,
	ctor:function (parent, tag) {
		this.tag = tag;
		this.layer = parent;
		this.scene = parent.getParent();
		this.init();
	},
	init:function (){
		this.rootNode = new cc.NodeGrid();
		this.rootNode.setScale(0.1);
		this.rootNode.setRotation(180);
		this.layer.addChild(this.rootNode, 99, this.tag);
		
		this.bg = new cc.Sprite("#lite.png");
		this.bg.setPosition(gg.c_p);
		this.rootNode.addChild(this.bg, 1,TAG_WIN_BG);
		
		this.rootNode.setVisible(false);
		this.initSp();
	},
	initSp:function (){
		// 初始化，自定义元素
	},
	open:function(){
		this.rootNode.setVisible(true);
		var scale = cc.scaleTo(0.5,1);
		var rotate = cc.rotateTo(0.5,0);
		var spawn = cc.spawn(scale,rotate);
		var sequence = cc.sequence(spawn);
		this.rootNode.runAction(sequence);
	},
	close:function(){
		var func = cc.callFunc(this.destroy, this);
		var scale = cc.scaleTo(0.5,0.1);
		var rotate = cc.rotateTo(0.5,180);
		var spawn = cc.spawn(scale, rotate);
		var sequence = cc.sequence(spawn, func);
		this.rootNode.runAction(sequence);
	},
	destroy:function() {
		if(this.closeNext){
			gg.flow.next();
		}
		gg.synch_listener = true;
		this.rootNode.setVisible(false);
	},
	closeWin:function(closeNext){
		// 关闭时，是否下一步
		if(closeNext == null){
			this.closeNext = true;
		} else {
			this.closeNext = closeNext;	
		}
		this.layer.scheduleOnce(this.close.bind(this),0.8);
	}
})