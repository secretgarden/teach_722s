/**
 * 一直存在的，俯视图
 */
C7Cloud = Cloud.extend({
	ctor:function (parent, tag) {
		this._super(parent, tag);
		this.loadData();
		this.initCloud();
	},
	loadData:function(){
	},
	initCloud:function(){
		this.bg2 = new cc.Sprite("#C7/bg.png"); 
		this.rootNode.addChild(this.bg2);
		this.img = new cc.Sprite("#C7/c1.png");
		this.rootNode.addChild(this.img);
		this.tip = new cc.LabelTTF("",gg.fontName,25);
		this.tip.setColor(cc.color(0,0,0));
		this.tip.setPosition(0, -170);
		this.rootNode.addChild(this.tip);
		this.open();
	},
	putZheg:/**
			 * 放入遮光体
			 */
		function(){
		this.tip.setString("拉动可以杆子使光路与遮光体对准");
		this.img.setSpriteFrame("C7/c2.png");
		this.delayClose();
	},
	outZheg:/**
			 * 取出遮光体
			 */
	function(){
		this.open();
		this.tip.setString(""); 
		this.img.setSpriteFrame("C7/c1.png");
		this.delayClose();
	},
	putBise1:function(){
		this.img.setSpriteFrame("C7/c3.png");
		this.delayClose();
	},
	putBise2:function(){
		this.tip.setString("拉动可以杆子使光路与遮光体对准");
		this.img.setSpriteFrame("C7/c4.png");
		this.delayClose();
	},
	putBise3:function(){
		this.tip.setString("拉动可以杆子使光路与遮光体对准");
		this.img.setSpriteFrame("C7/c5.png");
		this.delayClose();
	},
	putBise4:function(){
		this.tip.setString("拉动可以杆子使光路与遮光体对准");
		this.img.setSpriteFrame("C7/c6.png");
		this.delayClose();
	},
	delayClose:function(){
		this.rootNode.scheduleOnce(function(){
			this.close();
			gg.flow.next();
		}.bind(this),2);
	},
	destroy:function() {
		this.rootNode.setVisible(false);
	}
})