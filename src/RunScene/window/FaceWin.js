FaceWin = Window.extend({
	ctor:function (parent, tag) {
		this._super(parent, tag);
	},
	initSp:function (){
		var face = new Button(this.rootNode,
				d_z_index, TAG_722S_FACE,
				"#722s_side1.png",
				this.callbackSp,this);
		face.setPosition(gg.c_width, gg.c_height);

		var pole = new Button(this.rootNode,
				d_z_index, TAG_FACE_POLE,
				"#pole4.png",
				this.callbackSp,this);
		pole.setPosition(gg.c_width, gg.c_height);
	},
	openCap:function(){
		var animFrames = [];
		for (var i = 1; i < 5; i++) {
			var str = "722s_side" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation = new cc.Animation(animFrames, 0.1);
		var openAction = cc.animate(animation);
		
		var face = this.rootNode.getChildByTag(TAG_722S_FACE);
		var func = cc.callFunc(function(){
			face.setSFrame("722s_side4.png");
			gg.flow.next();
		},this);
		var seq = cc.sequence(openAction,func);
		face.stop();
		face.runAction(seq);
	},
	closeCap:function(mark){
		var animFrames = [];
		for (var i = 4; i > 0; i--) {
			var str = "722s_side" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation = new cc.Animation(animFrames, 0.1);
		var closeAction = cc.animate(animation);

		var face = this.rootNode.getChildByTag(TAG_722S_FACE);
		var func = cc.callFunc(function(){
			face.setSFrame("722s_side1.png");
			gg.flow.next();
			cc.log("合上盖子");
			if(mark == MARK_DO1){
				// 打开屏幕
				cc.log("打开屏幕");
				this.layer.screen.open();
			}
			gg.synch_listener = true;
		},this);
		var seq = cc.sequence(closeAction,func);
		face.stop();
		face.runAction(seq);
	},
	callbackSp:function(pSender){
		gg.synch_listener = false;
		var action = gg.flow.flow.action;
		cc.log("say:" + pSender.getTag() + "|action:" + action);
		switch (pSender.getTag()){
		case TAG_722S_FACE:
			if(action == ACTION_DO1){
				this.openCap();	
			} else if(action == ACTION_DO2){
				this.closeCap(MARK_DO1);
			} else if(action == ACTION_DO3){
				// 取出遮光体
				this.layer.c7.outZheg();
				var zheg = this.layer.getChildByTag(TAG_ITEM_ZHEG);
				zheg.setVisible(true);
				zheg.setLocalZOrder(200);
				var move = cc.moveTo(1, cc.p(550, 630));
				var fade = cc.fadeOut(0);
				var seq=cc.sequence(move,fade);
				zheg.runAction(seq);
			} else if(action == ACTION_DO4){
				// 合上盖子
				this.closeCap(MARK_DO1);
				// 打开屏幕
				this.layer.screen.label.setString("3033");
			} else if(action == ACTION_DO5){
				// 合上盖子
				this.closeCap();// MARK_DO2
			} else {
			}
			break;
		case TAG_FACE_POLE:
			if(action == ACTION_DO1){
				pSender.setSFrame("pole3.png");
				this.layer.screen.flashShow("0.235");
				this.layer.note.write("第一次   " + "0.235");
				this.layer.note.open();
				this.layer.note.delayClose();
				gg.flow.next();
			} else if(action == ACTION_DO2){
				pSender.setSFrame("pole2.png");
				this.layer.screen.flashShow("0.234");
				this.layer.note.write("第二次   " + "0.234");
				this.layer.note.open();
				this.layer.note.delayClose();
				gg.flow.next();
			} else if(action == ACTION_DO3){
				pSender.setSFrame("pole1.png");
				this.layer.screen.flashShow("0.237");
				this.layer.note.write("第三次   " + "0.237");
				this.layer.note.open2();
				this.closeWin();
			}
			// 打开屏幕
			this.layer.screen.open();
			break;
		}
	},
})