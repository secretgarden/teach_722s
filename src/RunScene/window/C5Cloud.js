/**
 * 润洗，添加待测液
 */
C5Cloud = Cloud.extend({
	ctor:function (parent, tag) {
		this._super(parent, tag);
		this.loadData();
		this.initCloud();
	},
	loadData:function(){
		this.tobe_cur = 1;
	},
	initCloud:function(){
		var armatureDataManager = ccs.armatureDataManager;
		armatureDataManager.addArmatureFileInfo(
				res_run.canbi_g,
				res_run.canbi_p,
				res_run.canbi_j);
		this.tobecheck = new ccs.Armature("canbi");
		this.tobecheck.setPosition(-70, -30);
		this.rootNode.addChild(this.tobecheck);

		this.text = new cc.MenuItemFont("蒸馏水润洗",this.callback,this);
		this.text.setColor(cc.color(0,0,0,0));
		this.text.setPosition(0, -80);
		
		var menu = new cc.Menu(this.text);
		menu.setPosition(0, 0);
		this.rootNode.addChild(menu);
		this.flashText();
		
		this.open();
	},
	callback:function(){
		this.text.setEnabled(false);
		if(this.tobe_cur < 4){
			ll.tip.mdScore(10);
			this.tobecheck.getAnimation().play("runxi");
			this.stopFlash();
			this.tobecheck.getAnimation().setFrameEventCallFunc(
					function(){
						this.text.setEnabled(true);
						this.flashText();
					},this);// "over_water"
		}else if(this.tobe_cur < 7){
			ll.tip.mdScore(10);
			this.tobecheck.getAnimation().play("runxi_blue");
			this.stopFlash();
			this.tobecheck.getAnimation().setFrameEventCallFunc(
					function(){
						this.text.setEnabled(true);
						this.flashText();
					},this);// "over_water"
		} else if(this.tobe_cur == 7){
			ll.tip.mdScore(10);
			this.tobecheck.getAnimation().play("add_blue");
			this.stopFlash();
			this.tobecheck.getAnimation().setFrameEventCallFunc(
					function(){
						this.text.setEnabled(true);
						this.text.setVisible(false);
						this.close();
						gg.flow.next();
					},this);// "over_runxi"
		}
		this.tobe_cur++;
		if(this.tobe_cur == 4){
			this.flashText();
			this.text.setString("待测溶液润洗");
			gg.flow.next();
		} else if(this.tobe_cur == 7){
			this.flashText();
			this.text.setString("加入待测溶液");
			gg.flow.next();
		}
	},
	flashText:function (){
		if(gg.teach_type == TAG_LEAD){
		gg.flow.doDemo(this.text);
		
		var fade1 = cc.fadeTo(0.5, 50);
		var fade2 = cc.fadeTo(0.5, 255);
		var seq = cc.sequence(fade1,fade2);
		var flash = cc.repeatForever(seq);
		this.text.runAction(flash);
		}
	},
	stopFlash:function (){
		this.text.stopAllActions();
		this.text.setOpacity(255);
	}
})