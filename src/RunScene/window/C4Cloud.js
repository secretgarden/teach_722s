/**
 * 摇匀比色管
 */
C4Cloud = Cloud.extend({
	ctor:function (parent, tag) {
		this._super(parent, tag);
		this.loadData();
		this.initCloud();
	},
	loadData:function(){
	},
	initCloud:function(){
		var guan = new Angel(this.rootNode,"#biseguan2.png",
				function(pSend){
			var rotate1 = cc.rotateTo(0.5, 180);
			var rotate2 = cc.rotateTo(0.5, 0);
			var seq = cc.sequence(rotate1,rotate2);
			var forever = cc.repeat(seq,3);
			var func = cc.callFunc(function(){
				gg.flow.next();
				this.layer.closeCloud();
				this.layer.cloud = new C5Cloud(this.layer,TAG_722S_CLOUD);
			}.bind(this), this);
			var seq2 = cc.sequence(forever,func);
			pSend.runAction(seq2);
		},this);
		guan.flash();
		this.open();
	}
})