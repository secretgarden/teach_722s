/**
 * 文字云
 */
FlashCloud = Cloud.extend({
	ctor:function (parent, tag) {
		this._super(parent, tag);
		this.loadData();
		this.initCloud();
	},
	loadData:function(){
		this.curScale = 1;
	},
	initCloud:function(){
		this.content = new cc.LabelBMFont("",res_run.font_f);
		this.content.setColor(cc.color(0,0,0,0));
		this.rootNode.addChild(this.content);
		this.rootNode.setLocalZOrder(999);
	},
	setPosition:function(pos){
		this.rootNode.setPosition(pos);
	},
	doFlash:function(str){
		this.open();
		this.content.setString(str);
		this.rootNode.scheduleOnce(
			function(){
			this.close();	
		}.bind(this), 3);
	}
})