SideWin = Window.extend({
	ctor:function (parent, tag) {
		this._super(parent, tag);
	},
	initSp:function (){
		var back = new Button(this.rootNode,
				d_z_index, TAG_722S_BACK,
				"#722s_back.png",
				this.callbackSp,this);
		back.setPosition(gg.c_width, gg.c_height);

		var start = new Button(this.rootNode,
				d_z_index, TAG_BUTTON_START,
				"#722s_open.png",
				this.callbackSp,this);
		start.setPosition(296, 341);
	},
	callbackSp:function(pSender){
		gg.synch_listener = false;
		cc.log("say:" + pSender.getTag());
		var action = gg.flow.flow.action;
		switch (pSender.getTag()){
			case TAG_BUTTON_START:
				// 开灯
				var body = this.layer.getChildByTag(TAG_ITEM_BODY)
				body.setSFrame("722s_light.png");
				this.closeWin();
				break;
		}
	}
})