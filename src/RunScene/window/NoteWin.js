NoteWin = cc.Sprite.extend({
	run_flag: false,
	open_flag: false,
	curTPos:null,
	count: 0,
	ctor:function (parent, name, zIndex) {
		this._super(name);
		parent.addChild(this, zIndex);
		this.setPosition(800,1200);
	},
	open:function(){
		if(this.run_flag){
			return;
		}
		this.run_flag = true;
		this.open_flag = true;
		var move = cc.moveTo(0.6,cc.p(800,520));
		var seq = cc.sequence(move,
			cc.callFunc(this.back,this));
		this.runAction(seq);
	},
	open2:/**
			 * 不释放点击同步
			 */
	function(){
		if(this.run_flag){
			return;
		}
		this.run_flag = true;
		this.open_flag = true;
		var move = cc.moveTo(0.6,cc.p(800,520));
		var seq = cc.sequence(move,
				cc.callFunc(this.back2,this));
		this.runAction(seq);		
	},
	close:function(){
		if(this.run_flag){
			return;
		}
		this.run_flag = true;
		this.open_flag = false;
		var move = cc.moveTo(1,cc.p(800,1200));
		var seq = cc.sequence(move,
				cc.callFunc(this.back,this));
		this.runAction(seq);
	},
	write:function(str){
		this.count ++;
		var text = new cc.LabelTTF(str,gg.fontName,gg.fontSize);
		var x = text.width * 0.5 + 5;
		if(this.curTPos == null){
			this.curTPos = cc.p(x, 110);
		} else {
			var newY = this.curTPos.y - text.height;
			this.curTPos = cc.p(x, newY);
		}
		text.setColor(cc.color(0,0,0));
		text.setPosition(this.curTPos);
		this.addChild(text);
	},
	delayClose:function(){
		this.scheduleOnce(function(){
			this.close();	
		}.bind(this), 5);
	},
	isOpen:function(){
		return this.open_flag;
	},
	back:function(){
		this.run_flag = false;
		gg.synch_listener = true;
	},
	back2:function(){
		this.run_flag = false;
	}
})
