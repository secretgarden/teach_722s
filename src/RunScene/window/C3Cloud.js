/**
 * 比色管，调试待测液
 */
C3Cloud = Cloud.extend({
	ctor:function (parent, tag) {
		this._super(parent, tag);
		this.loadData();
		this.initCloud();
	},
	loadData:function(){
		this.curBSG = 1;
		this.bsg_flag = true;
	},
	initCloud:function(){
		cc.spriteFrameCache.addSpriteFrames(res_run.biseguan_p);
		var armatureDataManager = ccs.armatureDataManager;
		armatureDataManager.addArmatureFileInfo(
				res_run.biseguan_g,
				res_run.biseguan_p,
				res_run.biseguan_j);
		this.bsg = new ccs.Armature("biseguan");
		this.bsg.setPosition(-80, -50);
		this.rootNode.addChild(this.bsg);

		this.text = new cc.MenuItemFont("配制",this.callBack,this);
		this.text.setColor(cc.color(0,0,0,0));
		this.text.setPosition(0, -120);
		this.flashText();

		var menu = new cc.Menu(this.text);
		menu.setPosition(0, 0);
		this.rootNode.addChild(menu);

		this.open();

	},
	callBack:function(){
		if(!this.bsg_flag){
			cc.log("冷却中");
			return;
		}
		this.bsg_flag = false;
		cc.log("配置");
		switch(this.curBSG){
		case 1:
			ll.tip.mdScore(10);
			this.bsg.getAnimation().play("peizhi1");	
			break;
		case 2:
			ll.tip.mdScore(10);
			this.bsg.getAnimation().play("peizhi2");
			break;
		case 3:
			ll.tip.mdScore(10);
			this.bsg.getAnimation().play("peizhi3");
			break;
		}
		this.stopFlash();
		this.curBSG ++;
		this.bsg.getAnimation().setFrameEventCallFunc(
			function(){
				this.bsg_flag = true;
				this.flashText();
				if(this.curBSG == 4){
					gg.flow.next();
					this.layer.closeCloud();
					this.layer.cloud = new C4Cloud(this.layer,TAG_722S_CLOUD);
				}
			},this);
	},
	flashText:function (){
		var fade1 = cc.fadeTo(0.5, 50);
		var fade2 = cc.fadeTo(0.5, 255);
		var seq = cc.sequence(fade1,fade2);
		var flash = cc.repeatForever(seq);
		this.text.runAction(flash);
	},
	stopFlash:function (){
		this.text.stopAllActions();
		this.text.setOpacity(255);
	}
})