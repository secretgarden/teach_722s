/**
 * 取出遮光体
 */
C6Cloud = Cloud.extend({
	ctor:function (parent, tag) {
		this._super(parent, tag);
		this.loadData();
		this.initCloud();
	},
	loadData:function(){
	},
	initCloud:function(){
		this.rootNode.setPosition(830, 380);
		
		cc.spriteFrameCache.addSpriteFrames(res_run.zhegt_out_p);
		var armatureDataManager = ccs.armatureDataManager;
		armatureDataManager.addArmatureFileInfo(
				res_run.zhegt_out_g,
				res_run.zhegt_out_p,
				res_run.zhegt_out_j);
		this.zhegt = new ccs.Armature("zhegt_out");
		this.rootNode.addChild(this.zhegt);
		this.zhegt.getAnimation().play("out");
		this.zhegt.getAnimation().setFrameEventCallFunc(
			function(){
				this.layer.c7.putZheg();
//				gg.flow.next();
				this.close();
			},this);
		this.open();
	}
})