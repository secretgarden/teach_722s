/**
 * 比色皿润洗，添加
 */
C2Cloud = Cloud.extend({
	ctor:function (parent, tag) {
		this._super(parent, tag);
		this.loadData();
		this.tipCloud();		
	},
	loadData:function(){
		this.runxi_cur = 1;
	},
	tipCloud:function(){
		this.tip = new cc.Sprite("#tip_cloud.png");
		this.rootNode.addChild(this.tip);
		this.open();
		this.rootNode.scheduleOnce(
			function(){
				this.tip.removeFromParent(true);
				this.initCloud();	
			}.bind(this), 3);
	},
	initCloud:function(){
		cc.spriteFrameCache.addSpriteFrames(res_run.canbi_p);
		var armatureDataManager = ccs.armatureDataManager;
		armatureDataManager.addArmatureFileInfo(
			res_run.canbi_g,
			res_run.canbi_p,
			res_run.canbi_j);
		this.water = new ccs.Armature("canbi");
		this.water.setPosition(-70, -30);
		this.rootNode.addChild(this.water);
		this.text = new cc.MenuItemFont("蒸馏水润洗",this.callback,this);
		this.text.setColor(cc.color(0,0,0,0));
		this.text.setPosition(0, -80);
		
		var menu = new cc.Menu(this.text);
		menu.setPosition(0, 0);
		this.rootNode.addChild(menu);
		this.flashText();
	},
	callback:function(){
		this.text.setEnabled(false);	
		if(this.runxi_cur < 4){
			ll.tip.mdScore(10);
			this.water.getAnimation().play("runxi");
			this.stopFlash();
			this.water.getAnimation().setFrameEventCallFunc(
					function(){
						this.text.setEnabled(true);
						this.flashText();
					},this);// "over_water"
		} else if(this.runxi_cur < 7){
			ll.tip.mdScore(10);
			this.water.getAnimation().play("runxi_white");
			this.stopFlash();
			this.water.getAnimation().setFrameEventCallFunc(
					function(){
						this.text.setEnabled(true);
						this.flashText();
					},this);// "over_water"
		} else if(this.runxi_cur == 7){
			ll.tip.mdScore(10);
			this.water.getAnimation().play("add_white");
			this.stopFlash();
			this.water.getAnimation().setFrameEventCallFunc(
				function(){
					this.text.setEnabled(true);
					this.text.setVisible(false);
					this.next();
				},this);// "over_runxi"
		}
		this.runxi_cur++;
		if(this.runxi_cur == 4){
			this.text.setString("参比溶液润洗");
			this.next();
		} else if(this.runxi_cur == 7){
			this.text.setString("加入参比溶液");
			this.next();
		}
	},
	next:function(){
		gg.flow.next();
	},
	flashText:function (){
		if(gg.teach_type == TAG_LEAD){
		gg.flow.doDemo(this.text);		
		var fade1 = cc.fadeTo(0.5, 50);
		var fade2 = cc.fadeTo(0.5, 255);
		var seq = cc.sequence(fade1,fade2);
		var flash = cc.repeatForever(seq);
		this.text.runAction(flash);
		}
	},
	stopFlash:function (){
		this.text.stopAllActions();
		this.text.setOpacity(255);
	}
})