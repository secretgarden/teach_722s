/**
 * 转动“旋钮”，刻度调整到510
 */
C1Cloud = Cloud.extend({
	ctor:function (parent, tag) {
		this._super(parent, tag);
		this.loadData();
		this.initCloud();
	},
	loadData:function(){
		this.curScale = 1;
	},
	initCloud:function(){
		this.bg = new cc.Sprite("#C1/beijing.png")
		this.lp = new cc.Sprite("#C1/lunpan.png");
		this.gazi = new cc.Sprite("#C1/gaizi.png");
		this.rootNode.addChild(this.bg,1);
		this.rootNode.addChild(this.gazi,3);
		this.rootNode.addChild(this.lp,2);
		this.lp.setPosition(-30, 0);
		this.lp.setRotation(-40);

		this.bt = new Angel(this.rootNode,"#C1/rotate.png",
				this.callBack,this);
		this.bt.flash();
		this.bt.setLocalZOrder(4);
		this.bt.setTag(TAG_CLOUD_ROTATE);
		this.open();
		
		gg.flow.doDemo(this.bt);
	},
	curDegree:580,
	do_flag:false,
	callBack:function(){
		this.bt.setEnable(false);
		this.bt.stop();
		if(this.curDegree <= 510){
			return;
		}
		var func = cc.callFunc(function(){
			this.bt.setEnable(true);
			gg.flow.doDemo(this.bt);
			if(this.curDegree <= 510){
				gg.flow.next();
				this.close();
			}
		}, this);
		
		var unit = 35;
		var r = cc.rotateBy(3,-unit);
		var seq = cc.sequence(r, func);
		this.lp.runAction(seq);
		this.curDegree -= unit * 2;
	}
})