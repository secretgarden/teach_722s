ScreenWin = Window.extend({
	ctor:function (parent, tag) {
		this._super(parent, tag);
	},
	initSp:function (){
		var back = new Button(this.rootNode,
				d_z_index, TAG_722S_SCREEN,
				"#sc.png",
				this.callbackSp,this);
		back.setPosition(gg.c_width, gg.c_height);

		var fun = new Button(this.rootNode,
				d_z_index, TAG_BUTTON_FUN,
				"#sc_b1.png",
				this.callbackSp,this);
		fun.setPosition(554, 331);
		
		var up = new Button(this.rootNode,
				d_z_index, TAG_BUTTON_UP,
				"#sc_b2.png",
				this.callbackSp,this);
		up.setPosition(612, 356);
		
		var down = new Button(this.rootNode,
				d_z_index, TAG_BUTTON_DOWN,
				"#sc_b3.png",
				this.callbackSp,this);
		down.setPosition(670, 356);
		
		var point = new cc.Sprite("#red_point.png");
		point.setPosition(425,380);
		this.rootNode.addChild(point,d_z_index,TAG_SCREEN_POINT);
		
		this.label = new cc.LabelTTF("0003",gg.fontName,gg.fontSize);
		this.label.setPosition(360,360);
		this.label.setColor(cc.color(0,0,0,0))
		this.rootNode.addChild(this.label,d_z_index);
	},
	flashShow:function(str){
		this.label.setString(str);
		this.rootNode.scheduleOnce(function(){
			this.closeWin();
		}.bind(this),3);
	},
	curPoint:1,
	pointDown:function(){
		this.curPoint ++;
		var point = this.rootNode.getChildByTag(TAG_SCREEN_POINT);
		var oPos = point.getPosition();
		if(this.curPoint >= 5){
			this.curPoint = 1;
			point.setPosition(425,380);
		} else {
			point.setPosition(oPos.x,oPos.y - 15);	
		}
	},
	callbackSp:function(pSender){
		gg.synch_listener = false;
		cc.log("say:" + pSender.getTag());
		var action = gg.flow.flow.action;
		switch (pSender.getTag()){
			case TAG_BUTTON_FUN:
				this.pointDown();
				if(action == ACTION_DO1 && this.curPoint == 2){
					this.closeWin();
					// 透射比,打开刻度盘
					this.layer.cloud = new C1Cloud(this.layer,TAG_722S_CLOUD);
				} else if(action == ACTION_DO2 && this.curPoint == 1){
					gg.flow.next();
					// 吸光度
				} else {
					// TODO 配合演示模式
					gg.flow.next();
				}
				break;
			case TAG_BUTTON_UP:
				gg.flow.next();
				// 100%
				this.label.setString("BL")
				this.rootNode.scheduleOnce(function(){
					gg.flow.next();
					this.label.setString("0000");
					this.rootNode.scheduleOnce(function(){
						this.layer.cloud = new C5Cloud(this.layer,TAG_722S_CLOUD);
						this.closeWin();
					}.bind(this),1);
				}.bind(this),2);
				break;
			case TAG_BUTTON_DOWN:
				// 调零
				this.label.setString("0000");
				this.closeWin();
				break;
		}
	}
})