var Game2MainLayer = cc.Layer.extend({
	lead:null,
	real:null,
	curSel:0,
	textArr: null,
	imageArr : null,
	tagArr1: null,
	tagArr2: null,
	tagVal: null,
	angelArr: [],
    ctor:function () {
        this._super();
        this.init();
        gg.game = this;
    },
    init:function(){
    	this.loadTurn();
    	this.loadTurnPoint();
    	this.loadBack();
    },
    loadTurn:function(){
    	this.turnTable = new cc.Sprite("#turntable.png");
    	this.turnTable.setPosition(gg.c_p);
    	this.turnTable.run_flag = false;
    	this.addChild(this.turnTable);
    },
    loadTurnPoint:function(){
    	this.turnPoint = new Angel(this,"#turnpoint.png",
    		function(){
    		if(!this.turnTable.run_flag){
    			cc.log("开始选题");
    			this.run();
    		}
    	},this);
    	this.turnPoint.setPosition(gg.c_p);
    },
    loadRun:function(){
    	// 初始速度
    	this.turnTable.speed=0;
    	// 减速度
    	this.turnTable.acc=0;    	
    },
    run:function(){
    	this.turnTable.speed=100 + $.getRandom(50);
    	this.turnTable.acc= -this.turnTable.speed * 0.03;  
    	cc.director.getScheduler().scheduleCallbackForTarget(
    			this.turnTable,this.runHand,0.1,cc.REPEAT_FOREVER,0,false);
    },
    runHand:function(){
    	this.run_flag = true;
    	this.speed+=this.acc
    	if(this.speed<0){
    		this.acc=0.1				
    	} else if(this.speed>0&&this.speed<0.2){
    		this.speed=0;
    		this.acc=0;
    	}
    	if(this.speed == 0){
    		var layer = this.getParent();
    		cc.director.getScheduler().unscheduleCallbackForTarget(this,layer.runHand);
// this.run_flag = false;
    		layer.showResult(this.getRotation());
			return;
		}
    	var rotate = cc.rotateBy(0.1,this.speed);	
    	this.runAction(rotate);
    },
    showResult:function(r){
    	var result = Math.ceil(r%360/60);
    	cc.log("题目"+ result);
    	this.cloud = new SelCloud(this, 9991, result);
    },
	loadBack:function(){
		var back = new Angel(this,"#game_back.png",function(){
			$.runScene(new StartScene());
		},this);
		back.setPosition(10 + back.width * 0.5, 10 + back.height * 0.5);
	},
	QuestionEnd:function(){
		this.scheduleOnce(function(){
			this.turnTable.run_flag = false;
		}.bind(this),1);
		
	}
});

