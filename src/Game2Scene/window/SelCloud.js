/**
 * 选择题界面
 */
TAG_T = 12000;
TAG_A = 12001;
TAG_B = 12002;
TAG_C = 12003;
TAG_D = 12004;
TAG_S = 12009;
SelCloud = Cloud.extend({
	serial:0,
	curTag:0,
	json: null,
	ctor:function (parent, tag, serial) {
		this._super(parent, tag);
		this.loadData(serial);
		this.initCloud();
	},
	loadData:function(serial){
		this.serial = serial % 4;
	},
	initCloud:function(){
		var json = testJson[this.serial];
		this.json = json;
		var titleLabel = new Label(this.rootNode,json.title, this.callback,this);
		titleLabel.setTag(TAG_T);
		titleLabel.setPosition(0,100);
		var A = new Label(this.rootNode,json.A,this.callback,this);
		A.setTag(TAG_A);
		A.setPosition(0,60);
		var B = new Label(this.rootNode,json.B,this.callback,this);
		B.setTag(TAG_B);
		B.setPosition(0,30);
		var C = new Label(this.rootNode,json.C,this.callback,this);
		C.setTag(TAG_C);
		var D = new Label(this.rootNode,json.D,this.callback,this);
		D.setTag(TAG_D);
		D.setPosition(0,-30);
		
		this.tip = new FlashTip(this.rootNode,"", res_run.font_f);
		this.tip.setPosition(0, -60);
		this.rootNode.addChild(this.tip);
		
		var sure = new Label(this.rootNode,"确定", this.callback,this);
		sure.setTag(TAG_S);
		sure.setPosition(0,-100);
		this.open();
	},
	callback:function(pSender){
		if(pSender.getTag() == TAG_S){
			if(this.curTag == 0){
				this.tip.doFlash("请选择答案");
			} else {
				this.close();
				this.layer.cloud = new ResultCloud(this.layer,9991,this.json,this.curTag);
			}
			return;
		}
		if(pSender.getTag() == TAG_T){
			return;
		}
		if(this.curTag != 0){
			this.rootNode.getChildByTag(this.curTag).stop();
		}
		pSender.flash();
		this.curTag = pSender.getTag();
	}
})

var testJson = [
    {
    	title:"调100%前，不用蒸馏水润洗比色皿，\n最终测量结果会如何？",
    	A : "A.数值将偏高",
    	B : "B.数值将偏低；",
    	C : "C.不影响测量精确度",
    	D : "D.有影响，具体影响视情况而定；",
    	code : TAG_A,
    	result : "A,未润洗将导致混进杂质，\n使得100%时的透光度变小，\n待测溶液的透光度占比变高而获得更高的读数。",
    },
    {
    	title:"测量待测溶液前，忘记使用待测溶液润洗比色皿，\n最终测量结果会如何？",
    	A : "A.数值将偏高",
    	B : "B.数值将偏低；",
    	C : "C.不影响测量精确度",
    	D : "D.有影响，具体影响视情况而定；",
    	code : TAG_B,
    	result : "B,测100%时残留的蒸馏水将稀释掉待润洗的溶液。",
    },
    {
    	title:"调100%前，没有擦干比色皿外面沾的水滴，\n最终测量结果会如何？",
    	A : "A.数值将偏高",
    	B : "B.数值将偏低；",
    	C : "C.不影响测量精确度",
    	D : "D.有影响，具体影响视情况而定；",
    	code : TAG_A,
    	result : "A,未擦干时外面的水珠会引起额外的折射和反射，\n使得100%时的透光度变小，\n待测溶液的透光度占比变高而获得更高的读数。",
    },
    {
    	title:"测量待测溶液前，没有擦干比色皿外面沾的溶液，\n最终测量结果会如何？",
    	A : "A.数值将偏高",
    	B : "B.数值将偏低；",
    	C : "C.不影响测量精确度",
    	D : "D.有影响，具体影响视情况而定；",
    	code : TAG_B,
    	result : "B,未擦干时外面的水珠会引起额外的折射和反射，\n使得透光度变小，使得读数变小。",
    }];