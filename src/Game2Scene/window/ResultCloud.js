/**
 * 结果云
 */
ResultCloud = Cloud.extend({
	sel : null,
	json: null,
	ctor:function (parent, tag, json, sel) {
		this._super(parent, tag);
		this.loadData(json, sel);
		this.initCloud();
	},
	loadData:function(json, sel){
		this.json = json;
		this.sel = sel;
		
	},
	initCloud:function(){
		this.loadTip();
		this.loadBack();
		this.layer.scheduleOnce(function(){
			this.open();
		}.bind(this),1);
	},
	loadBack:function(){
		var sure = new Label(this.rootNode,"返回",
			function(){
			this.layer.QuestionEnd();
			this.close();
		}.bind(this),this);
		sure.setTag(TAG_S);
		sure.setPosition(0,-100);
	},
	loadTip:function(){
		this.tip = new cc.LabelBMFont("", res_run.font_f);
		this.tip.setPosition(0, 80);
		this.tip.setColor(cc.color(0,0,0,0));
		this.rootNode.addChild(this.tip);
		
		this.tip2 = new cc.LabelBMFont("", res_run.font_f);
		this.tip2.setPosition(0, 0);
		this.tip2.setColor(cc.color(0,0,0,0));
		this.rootNode.addChild(this.tip2);
		
		if(this.json.code == this.sel){
			this.tip.setString("回答正确");
		} else {
			this.tip.setString("回答错误");
		}
		this.tip2.setString(this.json.result);
	},
	callback:function(pSender){
		if(pSender.getTag() == TAG_S){
			if(this.curTag == 0){
				this.tip.setString("请选择答案");
			} else {
				if(this.json.code == this.curTag){
					this.tip.setString("回答正确");
				} else {
					this.tip.setString("回答错误");
				}
				this.tip2.setString(this.json.result);
			}
			return;
		}
		if(pSender.getTag() == TAG_T){
			return;
		}
		if(this.curTag != 0){
			this.rootNode.getChildByTag(this.curTag).stop();
		}
		pSender.flash();
		this.curTag = pSender.getTag();
	}
})


var testJson = [
    {
    	title:"调100%前，不用蒸馏水润洗比色皿，\n最终测量结果会如何？",
    	A : "A.数值将偏高",
    	B : "B.数值将偏低；",
    	C : "C.不影响测量精确度",
    	D : "D.有影响，具体影响视情况而定；",
    	code : TAG_A,
    	result : "A,未润洗将导致混进杂质，\n使得100%时的透光度变小，\n待测溶液的透光度占比变高而获得更高的读数。",
    },
    {
    	title:"测量待测溶液前，忘记使用待测溶液润洗比色皿，\n最终测量结果会如何？",
    	A : "A.数值将偏高",
    	B : "B.数值将偏低；",
    	C : "C.不影响测量精确度",
    	D : "D.有影响，具体影响视情况而定；",
    	code : TAG_B,
    	result : "B,测100%时残留的蒸馏水将稀释掉待润洗的溶液。",
    },
    {
    	title:"调100%前，没有擦干比色皿外面沾的水滴，\n最终测量结果会如何？",
    	A : "A.数值将偏高",
    	B : "B.数值将偏低；",
    	C : "C.不影响测量精确度",
    	D : "D.有影响，具体影响视情况而定；",
    	code : TAG_A,
    	result : "A,未擦干时外面的水珠会引起额外的折射和反射，\n使得100%时的透光度变小，\n待测溶液的透光度占比变高而获得更高的读数。",
    },
    {
    	title:"测量待测溶液前，没有擦干比色皿外面沾的溶液，\n最终测量结果会如何？",
    	A : "A.数值将偏高",
    	B : "B.数值将偏低；",
    	C : "C.不影响测量精确度",
    	D : "D.有影响，具体影响视情况而定；",
    	code : TAG_B,
    	result : "B,未擦干时外面的水珠会引起额外的折射和反射，\n使得透光度变小，使得读数变小。",
    }];