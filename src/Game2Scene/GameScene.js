var Game2Layer = cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function () {
		this._super();
		this.initFrames();
		this.loadBackground();
		this.loadMainLayer();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(res_game.game_p);
		cc.spriteFrameCache.addSpriteFrames(res_run.run_p);
		cc.spriteFrameCache.addSpriteFrames(res_finish.finish_p);
	},
	loadBackground : function(){
		this.backgroundLayer = new Game2BackgroundLayer();
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new Game2MainLayer();
		this.addChild(this.mainLayar);
	}
});

var Game2Scene = PScene.extend({
	onEnter:function () {
		this._super();
		var layer = new Game2Layer();
		this.addChild(layer);
	}
});
