var StartMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
    ctor:function () {
        this._super();
// _.playBg();
        this.loadStartButton();
        this.loadVersion();
        return true;
    },
    loadVersion : function(){
    	var version = new cc.LabelTTF(gg.version, gg.fontName, fontSize);
    	version.setColor(cc.color(0,0,0,0));
    	version.setPosition(980, 25);
    	this.addChild(version);
    },
    loadStartButton : function(){
    	this.lead = new Angel(this,
        		"#home_lead1.png",
            this.callback);
    	this.lead.setHoverName("home_lead2.png");
    	this.lead.setTag(TAG_LEAD);
    	this.lead.setPosition(384, 100);
    	this.lead.setEnable(true);
        
    	this.real =  new Angel(this,
        		"#home_real1.png",
        		this.callback);
    	this.real.setHoverName("home_real2.png");
    	this.real.setTag(TAG_REAL);
    	this.real.setPosition(640, 100);
    	this.real.setEnable(true);
        
        this.xgame =  new Angel(this,
        		"#home_game1.png",
        		this.callback);
        this.xgame.setHoverName("home_game2.png");
        this.xgame.setTag(TAG_XGAME);
        this.xgame.setPosition(896, 100);
        this.xgame.setEnable(true);
        
        this.xgame2 =  new Angel(this,
        		"#home_game1.png",
        		this.callback);
        this.xgame2.setVisible(false);
        this.xgame2.setHoverName("home_game2.png");
        this.xgame2.setTag(TAG_XGAME2);
        this.xgame2.setPosition(gg.width * 0.5, 30);
        this.xgame2.setEnable(true);
        
        this.dataProcess = new Angel(this,
        		"#home_data1.png",
        		this.callback);
        this.dataProcess.setHoverName("home_data2.png");
        this.dataProcess.setTag(TAG_DATA_PROCESS);
        this.dataProcess.setPosition(gg.width * 0.5, 180);
        this.dataProcess.setEnable(true);
        	
        
        this.demo =  new Angel(this,
        		"#home_demo1.png",
        		this.callback);
        this.demo.setHoverName("home_demo2.png");
        this.demo.setTag(TAG_DEMO);
        this.demo.setPosition(128, 100);
        this.demo.setEnable(true);
    },
    callback:function(pSend){
    	gg.isdemo = false;
    	switch(pSend.getTag()){
    	case TAG_LEAD:
    		cc.log("进入引导模式");
    		gg.teach_type = TAG_LEAD;
    		this.runPlay();
    		break;
    	case TAG_REAL:
    		cc.log("进入实战模式");
    		gg.teach_type = TAG_REAL;
    		this.runPlay();
    		break;
    	case TAG_XGAME:
    		cc.log("进入小游戏模式");
    		this.runGame(TAG_XGAME);
    		break;
    	case TAG_XGAME2:
    		cc.log("进入小游戏模式");
    		this.runGame(TAG_XGAME2);
    		break;
    	case TAG_DEMO:
    		cc.log("进入演示模式");
    		gg.isdemo = true;
    		gg.teach_type = TAG_LEAD;
    		this.runPlay();
    		break;
    	case TAG_DATA_PROCESS:
    		this.runData();
    		break;
    	}
    },
    runPlay:function(){
    	if(gg.run_load){
    		$.runScene(new RunScene());	
    	} else {
    		if(this.tip == null){
    			this.tip = new cc.LabelTTF("加载中，请稍候再试",gg.fontName,gg.fontSize);
    			this.tip.setColor(cc.color(0,0,0,0));
    			this.tip.setPosition(gg.width * 0.5, gg.height * 0.5);
    			this.addChild(this.tip);
    		} else {
    			this.tip.setVisible(true);
    		}
    		this.scheduleOnce(function(){
    			this.tip.setVisible(false);	
    		}.bind(this), 1.5);
    	}
    },
    runGame:function(tag){
    	if(gg.game_load && gg.run_load && gg.finish_load){
    		if(tag == TAG_XGAME){
    			$.runScene(new GameScene());    			
    		} else {
    			$.runScene(new Game2Scene());
    		}
    	} else {
    		if(this.tip == null){
    			this.tip = new cc.LabelTTF("加载中，请稍候再试",gg.fontName,gg.fontSize);
    			this.tip.setColor(cc.color(0,0,0,0));
    			this.tip.setPosition(gg.width * 0.5, gg.height * 0.5)
    			this.addChild(this.tip);
    		} else {
    			this.tip.setVisible(true);
    		}
    		this.scheduleOnce(function(){
    			this.tip.setVisible(false);	
    		}.bind(this), 1.5);
    	}
    },
    runData:function(){
    	if(gg.game_load && gg.data_load){
   			$.runScene(new DataScene());    			
    	} else {
    		if(this.tip == null){
    			this.tip = new cc.LabelTTF("加载中，请稍候再试",gg.fontName,gg.fontSize);
    			this.tip.setColor(cc.color(0,0,0,0));
    			this.tip.setPosition(gg.width * 0.5, gg.height * 0.5)
    			this.addChild(this.tip);
    		} else {
    			this.tip.setVisible(true);
    		}
    		this.scheduleOnce(function(){
    			this.tip.setVisible(false);	
    		}.bind(this), 1.5);
    	}
    }
});
